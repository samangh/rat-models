/* Raccoon - An Electro-Magnetic and Thermal network 
solver accelerated with the Multi-Level Fast Multipole Method
Copyright (C) 2020  Jeroen van Nugteren*/

// include header
#include "serializer.hh"

// serializable objects
#include "rat/mlfmm/settings.hh"
#include "crosscircle.hh"
#include "crossdmsh.hh"
#include "crossrectangle.hh"
#include "crosspoint.hh"
#include "crossline.hh"
#include "modelcoil.hh"
#include "modelmesh.hh"
#include "modeltoroid.hh"
#include "modelgroup.hh"
#include "pathaxis.hh"
#include "pathcable.hh"
#include "pathcircle.hh"
#include "pathclover.hh"
#include "pathrectangle.hh"
#include "pathflared.hh"
#include "pathdshape.hh"
#include "patharc.hh"
#include "pathcct.hh"
#include "pathcctcustom.hh"
#include "pathoffset.hh"
#include "pathsub.hh"
#include "transbend.hh"
#include "transreflect.hh"
#include "transflip.hh"
#include "transreverse.hh"
#include "transrotate.hh"
#include "transtranslate.hh"
#include "driveac.hh"
#include "drivedc.hh"
#include "drivetrapz.hh"
#include "drivestep.hh"
#include "driveinterp.hh"
#include "calcgroup.hh"
#include "calcline.hh"
#include "calcharmonics.hh"
#include "calcgrid.hh"
#include "calcmesh.hh"
#include "calcsurface.hh"
#include "calcinductance.hh"
#include "calctracks.hh"
#include "calclength.hh"
#include "emitterbeam.hh"
#include "area.hh"
#include "frame.hh"
#include "mesh.hh"
#include "meshcoil.hh"
#include "driveptrain.hh"

// code specific to Raccoon
namespace rat{namespace mdl{
	
	// constructor
	Serializer::Serializer(){
		register_constructors();
	}

	// constructor
	Serializer::Serializer(const boost::filesystem::path &fname){
		register_constructors();
		import_json(fname);
	}

	// factory
	ShSerializerPr Serializer::create(){
		return std::make_shared<Serializer>();
	}

	// factory
	ShSerializerPr Serializer::create(const boost::filesystem::path &fname){
		return std::make_shared<Serializer>(fname);
	}

	// registry
	void Serializer::register_constructors(){
		// factories from MLFMM
		register_factory<fmm::Settings>();

		// cross sections
		register_factory<CrossCircle>();
		register_factory<CrossDMsh>();
		register_factory<CrossRectangle>();
		register_factory<CrossPoint>();
		register_factory<CrossLine>();

		// model nodes
		register_factory<ModelCoil>();
		register_factory<ModelMesh>();
		register_factory<ModelToroid>();
		register_factory<ModelGroup>();

		// path nodes
		register_factory<PathAxis>();
		register_factory<PathCable>();
		register_factory<PathCircle>();
		register_factory<PathClover>();
		register_factory<PathRectangle>();
		register_factory<PathFlared>();
		register_factory<PathDShape>();
		register_factory<PathArc>();
		register_factory<PathCCT>();
		register_factory<PathCCTCustom>(); 
		register_factory<PathOffset>(); 
		register_factory<PathSub>(); 
		register_factory<CCTHarmonicInterp>();

		// transformations
		register_factory<TransBend>();
		register_factory<TransFlip>();
		register_factory<TransReflect>();
		register_factory<TransReverse>();
		register_factory<TransRotate>();
		register_factory<TransTranslate>();

		// drives
		register_factory<DriveAC>();
		register_factory<DriveDC>();
		register_factory<DriveTrapz>();
		register_factory<DriveStep>();
		register_factory<DriveInterp>();
		register_factory<DrivePTrain>();

		// calculation
		register_factory<CalcGroup>();
		register_factory<CalcLine>();
		register_factory<CalcHarmonics>();
		register_factory<CalcGrid>();
		register_factory<CalcMesh>();
		register_factory<CalcSurface>();
		register_factory<CalcInductance>();
		register_factory<CalcTracks>();
		register_factory<CalcLength>();
		register_factory<EmitterBeam>();

		// data objects
		register_factory<Area>();
		register_factory<Frame>();
		register_factory<Mesh>();
		register_factory<MeshCoil>();
	}

}}