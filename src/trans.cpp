/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "trans.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// mesh transformation
	void Trans::apply_coords(arma::Mat<double> &) const{

		// nothing happens
		return;
	}

	// vector transformation
	void Trans::apply_vectors(
		const arma::Mat<double> &, arma::Mat<double> &, const char) const{

		// nothing happens
		return;
	}

	// mesh transformation
	void Trans::apply_mesh(arma::Mat<arma::uword> &, const arma::uword) const{

		// nothing happens
		return;
	}

	// coordinate transformation for field array
	void Trans::apply_coords(arma::field<arma::Mat<double> > &R) const{
		for(arma::uword i=0;i<R.n_elem;i++)
			apply_coords(R(i));
	}

	// vector transformation for field array
	void Trans::apply_vectors(
		const arma::field<arma::Mat<double> > &R, arma::field<arma::Mat<double> > &V, const char str) const{
		assert(R.n_elem==V.n_elem);
		for(arma::uword i=0;i<R.n_elem;i++)
			apply_vectors(R(i),V(i),str);
	}

}}