/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "pathsub.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathSub::PathSub(){

	}

	// default constructor
	PathSub::PathSub(ShPathPr base, const arma::uword idx1, const arma::uword idx2){
		set_base(base); set_idx(idx1,idx2);
	}

	// factory
	ShPathSubPr PathSub::create(){
		return std::make_shared<PathSub>();
	}

	// factory
	ShPathSubPr PathSub::create(ShPathPr base, const arma::uword idx1, const arma::uword idx2){
		return std::make_shared<PathSub>(base, idx1, idx2);
	}


	// set base 
	void PathSub::set_base(ShPathPr base){
		if(base==NULL)rat_throw_line("base path points to zero");
		base_ = base;
	}

	// set indexes
	void PathSub::set_idx(const arma::uword idx1, const arma::uword idx2){
		idx1_ = idx1; idx2_ = idx2;
	}


	// get frame
	ShFramePr PathSub::create_frame() const{
		// check if base path was set
		if(base_==NULL)rat_throw_line("base path is not set");

		// create frame
		ShFramePr frame = base_->create_frame();

		// copy coordinates
		const arma::field<arma::Mat<double> > R = frame->get_coords();
		const arma::field<arma::Mat<double> > L = frame->get_direction();
		const arma::field<arma::Mat<double> > D = frame->get_transverse();
		const arma::field<arma::Mat<double> > N = frame->get_normal();
		const arma::field<arma::Mat<double> > B = frame->get_block();
		const arma::Row<arma::uword> section = frame->get_section();
		const arma::Row<arma::uword> turn = frame->get_turn();
		const arma::uword num_section_base = frame->get_num_section_base();

		// check indexes
		if(idx1_>idx2_)rat_throw_line("index1 must be smaller than index2");
		if(idx1_>=R.n_cols)rat_throw_line("index out of bounds");
		if(idx2_>=R.n_cols)rat_throw_line("index out of bounds");

		// create offset frame
		ShFramePr sub_frame = Frame::create(
			R.cols(idx1_,idx2_),L.cols(idx1_,idx2_),
			N.cols(idx1_,idx2_),D.cols(idx1_,idx2_),
			B.cols(idx1_,idx2_));

		// conserve location
		sub_frame->set_location(
			section.cols(idx1_,idx2_), 
			turn.cols(idx1_,idx2_), 
			num_section_base);

		// apply transformations
		sub_frame->apply_transformations(trans_);

		// create new frame
		return sub_frame;
	}

	// get type
	std::string PathSub::get_type(){
		return "rat::mdl::pathsub";
	}

	// method for serialization into json
	void PathSub::serialize(Json::Value &js, cmn::SList &list) const{
		// type
		js["type"] = get_type();

		// properties
		js["base"] = cmn::Node::serialize_node(base_,list);
		js["idx1"] = (int)idx1_;
		js["idx2"] = (int)idx2_;
	}

	// method for deserialisation from json
	void PathSub::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		set_base(cmn::Node::deserialize_node<Path>(js["base"],list,factory_list,pth));
		set_idx(js["idx1"].asUInt64(), js["idx2"].asUInt64());
	}


}}