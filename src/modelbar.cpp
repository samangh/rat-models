/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "modelbar.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelBar::ModelBar(){

	}

	// constructor
	ModelBar::ModelBar(ShPathPr base, ShCrossPr crss){
		set_base(base); set_cross(crss);
	}


	// factory
	ShModelBarPr ModelBar::create(){
		return std::make_shared<ModelBar>();
	}

	// factory immediately setting base and cross section
	ShModelBarPr ModelBar::create(ShPathPr base, ShCrossPr crss){
		return std::make_shared<ModelBar>(base,crss);
	}

	// set number of turns
	void ModelBar::set_softening(const double softening){
		if(softening_<0)rat_throw_line("softening must be zero or larger");
		softening_ = softening;
	}

	// set number of gauss points
	void ModelBar::set_num_gauss_volume(const arma::sword num_gauss_volume){
		if(num_gauss_volume==0)rat_throw_line("number of gauss points must be positive");
		num_gauss_volume_ = num_gauss_volume;
	}

	// set number of gauss points
	void ModelBar::set_num_gauss_surface(const arma::sword num_gauss_surface){
		if(num_gauss_surface==0)rat_throw_line("number of gauss points must be positive");
		num_gauss_surface_ = num_gauss_surface;
	}
	
	// set magnetisation
	void ModelBar::set_magnetisation(const arma::Col<double>::fixed<3> &Mf){
		Mf_  = Mf;
	}

	// factory for mesh objects
	ShMeshPrList ModelBar::create_mesh() const{
		// allocate mesh data
		ShMeshBarPr mesh = MeshBar::create();

		// fill mesh data
		setup_mesh(mesh);

		// copy properties
		mesh->set_softening(softening_);
		mesh->set_magnetisation(Mf_);
		mesh->set_num_gauss_surface(num_gauss_surface_);
		mesh->set_num_gauss_volume(num_gauss_volume_);

		// combine into list
		ShMeshPrList meshes(1);
		meshes(0) = mesh;

		// return mesh data
		return meshes;
	}

	// factory for mesh objects
	ShEdgesPrList ModelBar::create_edge() const{
		// allocate edge data
		ShEdgesPr edge = Edges::create();

		// fill edge data
		setup_edge(edge);

		// combine into list
		ShEdgesPrList edges(1);
		edges(0) = edge;

		// return edge data
		return edges;
	}


	// get type
	std::string ModelBar::get_type(){
		return "rat::mdl::modelbar";
	}

	// method for serialization into json
	void ModelBar::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		ModelMesh::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["softening"] = softening_;
		js["num_gauss_volume"] = (int)num_gauss_volume_;
		js["num_gauss_surface"] = (int)num_gauss_surface_;
	}

	// method for deserialisation from json
	void ModelBar::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent objects
		ModelMesh::deserialize(js,list,factory_list,pth);

		// properties
		set_softening(js["softening"].asDouble());
		set_num_gauss_volume(js["num_gauss_volume"].asInt64());
		set_num_gauss_surface(js["num_gauss_surface"].asInt64());
	}

}}