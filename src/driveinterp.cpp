/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "driveinterp.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	DriveInterp::DriveInterp(){

	}

	// constructor
	DriveInterp::DriveInterp(const arma::Col<double> &ti, const arma::Col<double> &vi, const arma::uword id){
		set_id(id); set_interpolation_table(ti,vi);
	}

	// factory
	ShDriveInterpPr DriveInterp::create(){
		return std::make_shared<DriveInterp>();
	}

	// factory
	ShDriveInterpPr DriveInterp::create(const arma::Col<double> &ti, const arma::Col<double> &vi, const arma::uword id){
		return std::make_shared<DriveInterp>(ti,vi,id);
	}


	// set and get current
	void DriveInterp::set_interpolation_table(const arma::Col<double> &ti, const arma::Col<double> &vi){
		if(ti.n_elem!=vi.n_elem)rat_throw_line("number of elements must be the same");
		ti_ = ti; vi_ = vi;
	}

	// get drive scaling
	double DriveInterp::get_scaling(const double time) const{
		const arma::uword idx = arma::as_scalar(arma::find(ti_.head_rows(ti_.n_elem-1)<time && ti_.tail_rows(ti_.n_elem-1)>=time));
		return vi_(idx) + (time - ti_(idx))*((vi_(idx+1)-vi_(idx))/(ti_(idx+1)-ti_(idx)));
		// return cmn::Extra::interp1(ti_,vi_,time,"linear",true);
	}

	// get drive scaling
	// arma::Col<double> DriveInterp::get_scaling(const arma::Col<double> &times) const{
	// 	// use scaler drive scaling
	// 	arma::Col<double> v(times.n_elem);
	// 	for(arma::uword i=0;i<times.n_elem;i++)
	// 		v(i) = get_scaling(times(i));
	// 	return v;
	// }

	// get current derivative
	double DriveInterp::get_dscaling(const double time) const{
		// find index
		assert(!ti_.is_empty()); assert(!vi_.is_empty());
		assert(time<=ti_.back()); assert(time>ti_.front());
		const arma::uword idx = arma::as_scalar(arma::find(ti_.head_rows(ti_.n_elem-1)<time && ti_.tail_rows(ti_.n_elem-1)>=time));
		return (vi_(idx+1)-vi_(idx))/(ti_(idx+1)-ti_(idx));
	}

	// get times at which an inflection occurs
	arma::Col<double> DriveInterp::get_inflection_times() const{
		// check if there are inflection points
		if(ti_.n_elem>2){
			arma::Col<double> times =  ti_.rows(1,ti_.n_elem-2);
			times = times(arma::find(arma::join_vert(arma::Col<arma::uword>{1},
				(times.tail_rows(times.n_elem-1)-
				times.head_rows(times.n_elem-1))>1e-8)));
			return times;
		}
		else return arma::Col<double>{};
	}

	// get type
	std::string DriveInterp::get_type(){
		return "rat::mdl::driveinterp";
	}

	// method for serialization into json
	void DriveInterp::serialize(Json::Value &js, cmn::SList &list) const{
		Drive::serialize(js,list);
		js["type"] = get_type();
		
		// walk over timesteps and export interpolation table
		for(arma::uword i=0;i<ti_.n_elem;i++){
			js["ti"].append(ti_(i)); js["vi"].append(vi_(i));
		}
	}

	// method for deserialisation from json
	void DriveInterp::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		Drive::deserialize(js,list,factory_list,pth);
		// const arma::uword num_times = js["ti"].size();
		// ti_.set_size(num_times); vi_.set_size(num_times);
		// for(arma::uword i=0;i<num_times;i++){
		// 	ti_(i) = js["ti"].get(i,0).asDouble(); 
		// 	vi_(i) = js["vi"].get(i,0).asDouble();
		// }

		ti_.set_size(js["ti"].size()); vi_.set_size(js["vi"].size()); arma::uword i;
		if(ti_.n_elem!=vi_.n_elem)rat_throw_line("interpolation arrays do not have same size");
		i = 0; for(auto it = js["ti"].begin(); it!=js["ti"].end();it++,i++)ti_(i) = (*it).asDouble();
		i = 0; for(auto it = js["vi"].begin(); it!=js["vi"].end();it++,i++)vi_(i) = (*it).asDouble();

	}

}}