/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "pathfile.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathFile::PathFile(){

	}

	// constructor
	PathFile::PathFile(const std::string& file_name){
		set_file_name(file_name);
	}

	// factory
	ShPathFilePr PathFile::create(){
		return std::make_shared<PathFile>();
	}

	// factory
	ShPathFilePr PathFile::create(const std::string& file_name){
		return std::make_shared<PathFile>(file_name);
	}

	// set file name
	void PathFile::set_file_name(const std::string& file_name){
		file_name_ = file_name;
	}

	// setup coordinates and orientation vectors
	ShFramePr PathFile::create_frame() const{
		if(file_name_=="")rat_throw_line("file name is not set");

		arma::Mat<double> filedata; //position

		filedata.load(file_name_, arma::raw_ascii);

		//Lines have 9 numbers:
		//posx posy posz dirx diryx dirz outx outy outz
		//pos: position
		//dir: directon in which the cable is travelling
		//out: one of the normal vectors. The other is calculated using cross product.

		assert(filedata.n_cols == 9);
		int n = filedata.n_rows;
		//std::cout << "Found " << n << " rows" << std::endl;

		arma::Mat<double> R = filedata.cols(0, 2).t(); //position
		arma::Mat<double> D = filedata.cols(3, 5).t(); //direction
		arma::Mat<double> O = filedata.cols(6, 8).t(); //out

		filedata.clear();

		// normalize
		D = arma::normalise(D, 2, 0);
		O = arma::normalise(O, 2, 0);

		// transverse direction from cross product
		arma::Mat<double> N(3, n); //norm (3rd direction)
		N = rat::cmn::Extra::cross(D,O);
		N = arma::normalise(N, 2, 0);

		// create frame
		ShFramePr gen = Frame::create(R, D, O, N, 1);

		R.clear();
		D.clear();
		O.clear();
		N.clear();

		// transformations
		gen->apply_transformations(trans_);

		// create frame
		return gen;
	}

}}