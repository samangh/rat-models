/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "model.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// export edges to freecad file
	void Model::export_freecad(cmn::ShFreeCADPr freecad) const{
		// check opera
		if(freecad==NULL)rat_throw_line("opera points to NULL");

		// create edges
		ShEdgesPrList edges = create_edge();

		// export to freecad
		for(arma::uword i=0;i<edges.n_elem;i++)
			edges(i)->export_freecad(freecad);
	}

	// export edges to opera file
	void Model::export_opera(cmn::ShOperaPr opera) const{
		// check opera
		if(opera==NULL)rat_throw_line("opera points to NULL");

		// create edges
		ShEdgesPrList edges = create_edge();

		// export to freecad
		for(arma::uword i=0;i<edges.n_elem;i++)
			edges(i)->export_opera(opera);
	}

	// function for calculating mesh volume
	double Model::calc_total_volume() const{
		// create mesh list
		ShMeshPrList meshes = create_mesh();

		// calculate volume
		double V = 0;
		for(arma::uword i=0;i<meshes.n_elem;i++)
			V += meshes(i)->calc_total_volume();

		// return volume
		return V;
	}

	// function for calculating mesh volume
	double Model::calc_total_surface_area() const{
		// create mesh list
		ShMeshPrList meshes = create_mesh();

		// calculate volume
		double A = 0;
		for(arma::uword i=0;i<meshes.n_elem;i++)
			A += meshes(i)->calc_total_surface_area();

		// return volume
		return A;
	}

}}