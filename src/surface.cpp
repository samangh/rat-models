/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "surface.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	Surface::Surface(){

	}

	// default constructor
	Surface::Surface(
		const arma::Mat<double> &R, const arma::Mat<double> &L,
		const arma::Mat<double> &N, const arma::Mat<double> &D,
		const arma::Mat<arma::uword> &s){
		set_mesh(R,s); set_longitudinal(L); set_normal(N); set_transverse(D);
	}

	// factory
	ShSurfacePr Surface::create(){
		return std::make_shared<Surface>();
	}

	ShSurfacePr Surface::create(
		const arma::Mat<double> &R, const arma::Mat<double> &L,
		const arma::Mat<double> &N, const arma::Mat<double> &D,
		const arma::Mat<arma::uword> &s){
		return std::make_shared<Surface>(R,L,N,D,s);
	}

	// set area 
	void Surface::set_area(ShAreaPr area){
		area_ = area;
	}

	// set frame 
	void Surface::set_frame(ShFramePr gen){
		gen_ = gen;
	}

	// set operating temperature
	void Surface::set_operating_temperature(const double operating_temperature){
		operating_temperature_ = operating_temperature;
	}

	// set mesh
	void Surface::set_mesh(const arma::Mat<double> &R, const arma::Mat<arma::uword> &s){
		assert(R.n_cols>s.max()); assert(R.n_rows == 3);
		assert(s.n_rows == 4 || s.n_rows==2);
		s_ = s; R_ = R;
	}

	// set longitudinal vector
	void Surface::set_longitudinal(const arma::Mat<double> &L){
		assert(L.n_rows==3);
		L_ = L;
	}

	// set transverse vector
	void Surface::set_transverse(const arma::Mat<double> &D){
		assert(D.n_rows==3);
		D_ = D;
	}

	// set normal vectors
	void Surface::set_normal(const arma::Mat<double> &N){
		assert(N.n_rows==3);
		N_ = N;
	}

	// get coordinates for section
	arma::Mat<double> Surface::get_direction() const{
		return L_;
	}

	// get coordinates for section
	arma::Mat<double> Surface::get_normal() const{
		return N_;
	}

	// get coordinates for section
	arma::Mat<double> Surface::get_transverse() const{
		return D_;
	}

	// set drive ID
	void Surface::set_drive_id(const arma::uword drive_id){
		drive_id_ = drive_id;
	}

	// set drive ID
	arma::uword Surface::get_drive_id() const{
		return drive_id_;
	}

	// get coordinates for section
	arma::Mat<double> Surface::get_coords() const{
		return R_;
	}

	// get coordinates for section
	arma::Mat<arma::uword> Surface::get_elements() const{
		return s_;
	}

	// get number of nodes
	arma::uword Surface::get_num_nodes() const{
		return R_.n_cols;
	}

	// get number of elements
	arma::uword Surface::get_num_elements() const{
		return s_.n_cols;
	}

	// get curret density at nodes
	arma::Mat<double> Surface::get_nodal_current_density() const{
		return arma::Mat<double>(3,get_num_nodes(),arma::fill::zeros);
	}

	// get temperature at nodes
	arma::Mat<double> Surface::get_nodal_temperature() const{
		return arma::Row<double>(get_num_nodes(),arma::fill::ones)*operating_temperature_;
	}

	// calculate field angle
	arma::Row<double> Surface::calc_alpha(const arma::Mat<double> &) const{
		return arma::Row<double>(get_num_nodes(),arma::fill::zeros);
	}

	// calculate engineering current density
	arma::Row<double> Surface::calc_Je(const arma::Mat<double> &, const arma::Row<double> &) const{
		return arma::Row<double>(get_num_nodes(),arma::fill::zeros);
	}

	// calculate engineering current density
	arma::Row<double> Surface::calc_Tc(const arma::Mat<double> &, const arma::Mat<double> &) const{
		return arma::Row<double>(get_num_nodes(),arma::fill::zeros);
	}

	// calculate electric field
	arma::Mat<double> Surface::calc_E(const arma::Mat<double> &, const arma::Mat<double> &, const arma::Row<double> &) const{
		return arma::Mat<double>(3,get_num_nodes(),arma::fill::zeros);
	}

}}