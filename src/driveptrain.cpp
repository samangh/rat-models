/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "driveptrain.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	DrivePTrain::DrivePTrain(){

	}

	// constructor
	DrivePTrain::DrivePTrain(
		const arma::uword num_pulses,
		const double amplitude, const double increment,
		const double tpulse, const double tstart, 
		const double tdelay, const arma::uword id){
		set_num_pulses(num_pulses);
		set_amplitude(amplitude); set_increment(increment); 
		set_tstart(tstart); set_tdelay(tdelay); 
		set_tpulse(tpulse); set_id(id);
	}

	// factory
	ShDrivePTrainPr DrivePTrain::create(){
		return std::make_shared<DrivePTrain>();
	}

	// factory
	ShDrivePTrainPr DrivePTrain::create(
		const arma::uword num_pulses,
		const double amplitude, const double increment,
		const double tpulse, const double tstart, 
		const double tdelay, const arma::uword id){
		return std::make_shared<DrivePTrain>(num_pulses, amplitude, increment, tpulse, tstart, tdelay, id);
	}

	// get scaling
	double DrivePTrain::get_scaling(const double time) const{
		const arma::Mat<double> ti = arma::reshape(get_inflection_times(), 2, num_pulses_);
		assert(arma::all(ti.row(1)>ti.row(0)));
		double scale = 0.0; 
		arma::Row<arma::uword> idx = arma::find(time>ti.row(0) && time<=ti.row(1));
		if(!idx.is_empty()){
			assert(idx.n_elem==1);
			scale = amplitude_ + arma::as_scalar(idx)*increment_;
		}
		return scale;
	}

	// get current derivative
	double DrivePTrain::get_dscaling(const double /*time*/) const{
		return 0;
	}

	// set amplitude of the pulse
	void DrivePTrain::set_amplitude(const double amplitude){
		amplitude_ = amplitude;
	}

	// set start time
	void DrivePTrain::set_tstart(const double tstart){
		tstart_ = tstart;
	}

	// set pulse duration
	void DrivePTrain::set_tpulse(const double tpulse){
		tpulse_ = tpulse;
	}
	
	// set time delay between pulses
	void DrivePTrain::set_tdelay(const double tdelay){
		tdelay_ = tdelay;
	}

	// set pulse increment
	void DrivePTrain::set_increment(const double increment){
		increment_ = increment;
	}

	// set number of pulses
	void DrivePTrain::set_num_pulses(const arma::uword num_pulses){
		num_pulses_ = num_pulses;
	}

	// get times at which an inflection occurs
	arma::Col<double> DrivePTrain::get_inflection_times() const{
		arma::Col<double> tinflection(2*num_pulses_);
		for(arma::uword i=0;i<num_pulses_;i++){
			tinflection(2*i) = tstart_ + i*(tdelay_+tpulse_);
			tinflection(2*i+1) = tstart_ + i*(tdelay_+tpulse_) + tpulse_;
		}
		return tinflection;
	}

	// get type
	std::string DrivePTrain::get_type(){
		return "rat::mdl::driveptrain";
	}

	// method for serialization into json
	void DrivePTrain::serialize(Json::Value &js, cmn::SList &list) const{
		Drive::serialize(js,list);
		js["type"] = get_type();
		js["amplitude"] = amplitude_;
		js["tstart"] = tstart_;
		js["tpulse"] = tpulse_;
		js["tdelay"] = tdelay_;
		js["increment"] = increment_;
		js["num_pulses"] = (int)num_pulses_;
	}

	// method for deserialisation from json
	void DrivePTrain::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		Drive::deserialize(js,list,factory_list,pth);
		set_amplitude(js["amplitude"].asDouble());
		set_tstart(js["tstart"].asDouble());
		set_tpulse(js["tpulse"].asDouble());
		set_tdelay(js["tdelay"].asDouble());
		set_increment(js["increment"].asDouble());
		set_num_pulses(js["num_pulses"].asUInt64());
	}

}}