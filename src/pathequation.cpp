/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "pathequation.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathEquation::PathEquation(){

	}

	// constructor
	PathEquation::PathEquation(const double element_size, CoordFun fun){
		set_coord_fun(fun); set_element_size(element_size);
	}

	// factory
	ShPathEquationPr PathEquation::create(){
		return std::make_shared<PathEquation>();
	}

	// factory
	ShPathEquationPr PathEquation::create(const double element_size, CoordFun fun){
		return std::make_shared<PathEquation>(element_size, fun);
	}

	// set element size 
	void PathEquation::set_element_size(const double element_size){
		if(element_size<=0)rat_throw_line("element size must be larger than zero");
		element_size_ = element_size;
	}

	// set divisor
	void PathEquation::set_element_divisor(const arma::uword element_divisor){
		if(element_divisor<=0)rat_throw_line("divisor must be larger than zero");
		element_divisor_ = element_divisor;
	}

	// set coordinate function
	void PathEquation::set_coord_fun(CoordFun fun){
		coord_fun_ = fun;
	}

	// set number of sections
	void PathEquation::set_num_sections(const arma::uword num_sections){
		if(num_sections<1)rat_throw_line("number of sections must be larger than one");
		num_sections_ = num_sections;
	}

	// setup coordinates and orientation vectors
	ShFramePr PathEquation::create_frame() const{
		if(element_size_==0)rat_throw_line("element size is not set");
		if(coord_fun_==NULL)rat_throw_line("coordinate function is not set");

		// calculate length
		const double ell = calc_length();

		// calculate number of times needed
		arma::uword num_times = std::ceil((double)std::max(2,(int)std::ceil(ell/element_size_))/num_sections_)+1;
		while((num_times-1)%element_divisor_!=0)num_times++;
		num_times *= num_sections_;

		// create times
		const arma::Row<double> t = arma::linspace<arma::Row<double> >(0.0,1.0,num_times);

		// allocate coordinates and direction
		arma::Mat<double> R, L, D;

		// calculate coordinates
		coord_fun_(R,L,D,t);

		// check output of user supplied coordinate function
		check_user_fun_output(R,L,D,t);
			
		// calculate normal vector
		const arma::Mat<double> N = cmn::Extra::cross(L,D);

		// create frame
		ShFramePr gen = Frame::create(R,L,N,D,num_sections_);

		// transformations
		gen->apply_transformations(trans_);

		// create frame
		return gen;
	}

	// calculate length
	double PathEquation::calc_length() const{
		// start values
		double ell = 0;
		arma::uword num_steps = num_steps_ini_;

		// allocate coordinates and direction
		arma::Mat<double> R, L, D;

		// keep iterations untill length constant
		for(arma::uword i=0;i<max_iter_;i++){
			// create times
			const arma::Row<double> t = arma::linspace<arma::Row<double> >(0.0,1.0,num_steps);

			// calculate coordinates
			coord_fun_(R,L,D,t);

			// check output of user supplied coordinate function
			check_user_fun_output(R,L,D,t);

			// numerically calculate length
			double ell_new = arma::accu(
				cmn::Extra::vec_norm(R.tail_cols(num_steps-1) - 
				R.head_cols(num_steps-1)));

			// check stop criteion
			if(std::abs(ell-ell_new)<ell_tol_)break;

			// double number of steps and update ell
			num_steps*=2; ell = ell_new;
		}

		// return calculated length
		return ell;
	}

	// check output of user supplied coordinate function
	// no we do not thrust the user at all
	void PathEquation::check_user_fun_output(
		const arma::Mat<double> &R, const arma::Mat<double> &L, 
		const arma::Mat<double> &D, const arma::Row<double> &t) const{ 
		const arma::uword num_steps = t.n_elem;
		if(R.n_rows!=3)rat_throw_line("coordinate matrix must have three rows");
		if(L.n_rows!=3)rat_throw_line("longitudinal vector matrix must have three rows");
		if(D.n_rows!=3)rat_throw_line("transverse vector matrix must have three rows");
		if(R.n_cols!=num_steps)rat_throw_line("coordinate matrix must match number of steps");
		if(L.n_cols!=num_steps)rat_throw_line("longitudinal vector matrix must match number of steps");
		if(D.n_cols!=num_steps)rat_throw_line("transverse vector matrix must match number of steps");
		if(!R.is_finite())rat_throw_line("coordinate matrix must be finite");
		if(!L.is_finite())rat_throw_line("longitudinal vector matrix must be finite");
		if(!D.is_finite())rat_throw_line("transverse vector matrix must be finite");
	}

}}