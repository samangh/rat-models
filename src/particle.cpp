/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "particle.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// create particle
	Particle::Particle(){

	}

	// set mass
	void Particle::set_rest_mass(const double rest_mass){
		rest_mass_ = rest_mass;
	}

	// set charge
	void Particle::set_charge(const double charge){
		charge_ = charge;
	}

	// get mass
	double Particle::get_rest_mass() const{
		return rest_mass_;
	}

	// get charge
	double Particle::get_charge() const{
		return charge_;
	}

	// set number of steps
	void Particle::set_num_steps(const arma::uword num_steps){
		if(num_steps<=0)rat_throw_line("number of steps must be larger than zero");
		num_steps_ = num_steps;
	}

	// set start index
	void Particle::set_start_index(const arma::uword start_idx){
		// start index
		idx_max_ = start_idx; idx_min_ = start_idx;
	}

	// set start coordinate
	void Particle::set_startcoord(
		const arma::Col<double>::fixed<3> &R0, 
		const arma::Col<double>::fixed<3> &V0){
		R0_ = R0; V0_ = V0;
	}

	// allocate
	void Particle::setup(){
		// check input
		if(idx_min_!=idx_max_)rat_throw_line("track must start and end at same index");
		if(idx_max_>num_steps_-1)rat_throw_line("start index exceeds number of steps");
		if(num_steps_==0)rat_throw_line("must have at least one step");

		// allocate storage
		Rt_.set_size(3,num_steps_); Vt_.set_size(3,num_steps_); t_.set_size(num_steps_);
		//Lt_.set_size(3,num_steps_); Nt_.set_size(3,num_steps_); Dt_.set_size(3,num_steps_);
		
		// set start point and velocity
		Rt_.col(idx_max_) = R0_; Vt_.col(idx_max_) = V0_; t_(idx_max_) = 0;
		
		// set allive
		alive_start_ = true; alive_end_ = true;
		if(idx_min_==0)alive_start_ = false;
		if(idx_max_==num_steps_-1)alive_end_ = false;
	}

	// check if the track start is alive
	bool Particle::is_alive_start() const{
		return alive_start_;
	}

	// check if the track end is alive
	bool Particle::is_alive_end() const{
		return alive_end_;
	}

	// get coordinate of track start
	arma::Col<double>::fixed<3> Particle::get_first_coord() const{
		return Rt_.col(idx_min_);
	}

	// get velocity of track start
	arma::Col<double>::fixed<3> Particle::get_first_velocity() const{
		return Vt_.col(idx_min_);
	}

	// get time
	double Particle::get_first_time() const{
		return t_(idx_min_);
	}

	// get coordinate of track end
	arma::Col<double>::fixed<3> Particle::get_last_coord() const{
		return Rt_.col(idx_max_);
	}

	// get velocity of track end 
	arma::Col<double>::fixed<3> Particle::get_last_velocity() const{
		return Vt_.col(idx_max_);
	}

	// get time
	double Particle::get_last_time() const{
		return t_(idx_max_);
	}

	// set coordinates
	void Particle::set_prev_coord(const double tp, const arma::Col<double>::fixed<3> &Rp, const arma::Col<double>::fixed<3> &Vp){
		// check if there is still space
		if(idx_min_==0)rat_throw_line("zero index reached (dead)");
		
		// update index
		idx_min_--;

		// store data at new index
		t_(idx_min_) = tp; 
		Rt_.col(idx_min_) = Rp; 
		Vt_.col(idx_min_) = Vp; 

		// check alive
		if(idx_min_==0)alive_start_ = false;
	}

	// set coordinates
	void Particle::set_next_coord(const double tp, const arma::Col<double>::fixed<3> &Rp, const arma::Col<double>::fixed<3> &Vp){
		// check if there is space left
		if(idx_max_==num_steps_-1)rat_throw_line("max index reached (dead)");

		// update index
		idx_max_++;

		// store data at new index
		t_(idx_max_) = tp; 
		Rt_.col(idx_max_) = Rp; 
		Vt_.col(idx_max_) = Vp;

		// check alive
		if(idx_max_==num_steps_-1)alive_end_ = false;
	}

	// terminate end
	void Particle::terminate_end(){
		alive_end_ = false;
	}

	// terminate start
	void Particle::terminate_start(){
		alive_start_ = false;
	}

	// get all track coordinates
	arma::Mat<double> Particle::get_track_coords() const{
		return Rt_.cols(idx_min_,idx_max_);
	}

	// get all track velocities
	arma::Mat<double> Particle::get_track_velocities() const{
		return Vt_.cols(idx_min_,idx_max_);
	}

}}
