/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "pathconnect.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathConnect::PathConnect(){

	}

	// constructor with dimension input
	PathConnect::PathConnect(
		ShPathPr prev_path, ShPathPr next_path, 
		const double str1, const double str2, 
		const double str3, const double str4, 
		const double ell_trans, 
		const double element_size){

		//check input
		assert(prev_path!=NULL);
		assert(next_path!=NULL);
		assert(ell_trans>=0);
		assert(element_size>0);

		// store to self
		prev_path_ = prev_path;
		next_path_ = next_path;
		
		// set strengths
		str1_ = str1; str2_ = str2; 
		str3_ = str3; str4_ = str4;

		// set transition length
		ell_trans_ = ell_trans;

		// set element size
		element_size_ = element_size;
	}


	// factory
	ShPathConnectPr PathConnect::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<PathConnect>();
	}

	// factory with dimension input
	ShPathConnectPr PathConnect::create(
		ShPathPr prev_path, ShPathPr next_path, 
		const double str1, const double str2, 
		const double str3, const double str4, 
		const double ell_trans, 
		const double element_size){
		//return ShPathCirclePr(new PathCircle(radius,dl));
		return std::make_shared<PathConnect>(prev_path,next_path,str1,str2,str3,str4,ell_trans,element_size);
	}

	// method for setting previous path
	void PathConnect::set_prev_path(ShPathPr prev_path){
		assert(prev_path!=NULL);
		prev_path_ = prev_path;
	}

	// method for setting next path
	void PathConnect::set_next_path(ShPathPr next_path){
		assert(next_path!=NULL);
		next_path_ = next_path;
	}

	// set strengths
	void PathConnect::set_strengths(
		const double str1, const double str2, 
		const double str3, const double str4){
		str1_ = str1; str2_ = str2;
		str3_ = str3; str4_ = str4;
	}

	// set transition length
	void PathConnect::set_ell_trans(
		const double ell_trans){
		assert(ell_trans>0);
		ell_trans_ = ell_trans;
	}

	// set element size
	void PathConnect::set_element_size(
		const double element_size){
		element_size_ = element_size;
	}

	// update function
	ShFramePr PathConnect::create_frame() const{
		// check connectors
		assert(prev_path_!=NULL);
		assert(next_path_!=NULL);

		// create frame
		ShFramePr prev_gen = prev_path_->create_frame();
		ShFramePr next_gen = next_path_->create_frame();

		// get frame of first path
		const arma::field<arma::Mat<double> > R0 = prev_gen->get_coords();
		const arma::field<arma::Mat<double> > L0 = prev_gen->get_direction();
		const arma::field<arma::Mat<double> > N0 = prev_gen->get_normal();
		//const arma::field<arma::Mat<double> > D0 = prev_gen->get_transverse();
		
		// get frame of second path
		const arma::field<arma::Mat<double> > R1 = next_gen->get_coords();
		const arma::field<arma::Mat<double> > L1 = next_gen->get_direction();
		const arma::field<arma::Mat<double> > N1 = next_gen->get_normal();
		//const arma::field<arma::Mat<double> > D1 = next_gen->get_transverse();

		// start and end vectors
		const arma::uword num_first = R0.n_elem;
		const arma::Col<double>::fixed<3> r0 = R0(num_first-1).tail_cols(1);
		const arma::Col<double>::fixed<3> l0 = L0(num_first-1).tail_cols(1);;
		const arma::Col<double>::fixed<3> n0 = N0(num_first-1).tail_cols(1);;
		const arma::Col<double>::fixed<3> r1 = R1(0).head_cols(1);;
		const arma::Col<double>::fixed<3> l1 = L1(0).head_cols(1);;
		const arma::Col<double>::fixed<3> n1 = N1(0).head_cols(1);;

		// create generator pointer
		ShFramePr gen;

		// cubic spline with twist
		if(str3_==0 && str4_==0){
			// check connection
			ShPathBSplinePr bezier = PathBSpline::create(
				r0, l0, n0, r1, l1, n1, str1_, str2_, element_size_);

			// return frame for this path
			gen = bezier->create_frame();
		}

		// quintic spline with constant perimeter
		else{
			// check connection
			ShPathBezierPr bezier = PathBezier::create(
				r0, l0, n0, r1, l1, n1, str1_, str2_, 
				str3_, str4_, ell_trans_, element_size_);

			// return frame for this path
			gen = bezier->create_frame();
		}

		// transfer type
		gen->set_conductor_type(prev_gen->get_conductor_type());

		// create generator list
		ShFramePrList genlist(3);
		genlist(0) = prev_gen; genlist(1) = gen; genlist(2) = next_gen;

		// combine frame and return
		return Frame::create(genlist);
	}

}}