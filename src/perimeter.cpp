/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "perimeter.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	Perimeter::Perimeter(){

	}

	// constructor with input
	Perimeter::Perimeter(
		const arma::Mat<double> &Rn, 
		const arma::Mat<arma::uword> &n){
		
		// set mesh
		set_mesh(Rn,n);	
	}

	// factory
	ShPerimeterPr Perimeter::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<Perimeter>();
	}

	// factory with dimension input
	ShPerimeterPr Perimeter::create(
		const arma::Mat<double> &Rn, 
		const arma::Mat<arma::uword> &n){
		return std::make_shared<Perimeter>(Rn,n);
	}

	// get number of elements
	arma::uword Perimeter::get_num_elements() const{
		return n_.n_cols;
	}

	// get number of nodes
	arma::uword Perimeter::get_num_nodes() const{
		return Rn_.n_cols;
	}

	// get node coordinates
	arma::Mat<double> Perimeter::get_nodes() const{
		return Rn_;
	}

	// get elements
	arma::Mat<arma::uword> Perimeter::get_elements() const{
		return n_;
	}

	// method for setting the mesh
	void Perimeter::set_mesh(
		const arma::Mat<double> &Rn, 
		const arma::Mat<arma::uword> &n){

		// check mesh
		assert(n.n_rows==2);
		assert(arma::max(arma::max(n))<Rn.n_cols);

		// set to self
		Rn_ = Rn; n_ = n;
	}

	// calculate length of periphery
	double Perimeter::calculate_length() const{
		// calculate edge vectors
		const arma::Mat<double> dR = 
			Rn_.cols(n_.row(1))-Rn_.cols(n_.row(0));

		// calculate length, sum and return
		return arma::as_scalar(arma::sum(arma::sqrt(arma::sum(dR%dR,0)),1));
	}

}}