/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "background.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	Background::Background(){

	}

	// constructor with input
	Background::Background(const arma::uword drive_id, const arma::Col<double>::fixed<3> &Hext){
		set_drive_id(drive_id); set_magnetic(Hext);
	}

	// factory
	ShBackgroundPr Background::create(){
		return std::make_shared<Background>();
	}
	
	// factory
	ShBackgroundPr Background::create(const arma::uword drive_id, const arma::Col<double>::fixed<3> &Hext){
		return std::make_shared<Background>(drive_id, Hext);
	}

	// set drive id
	void Background::set_drive_id(const arma::uword drive_id){
		drive_id_ = drive_id;
	}

	// get drive id
	arma::uword Background::get_drive_id() const{
		return drive_id_;
	}

}}