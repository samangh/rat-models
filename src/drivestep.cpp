/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "drivestep.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	DriveStep::DriveStep(){

	}

	// constructor
	DriveStep::DriveStep(const double amplitude, const double tstart, const double tpulse, const arma::uword id){
		set_amplitude(amplitude); set_tstart(tstart); set_tpulse(tpulse); set_id(id);
	}

	// factory
	ShDriveStepPr DriveStep::create(){
		return std::make_shared<DriveStep>();
	}

	// factory
	ShDriveStepPr DriveStep::create(const double amplitude, const double tstart, const double tpulse, const arma::uword id){
		return std::make_shared<DriveStep>(amplitude,tstart,tpulse,id);
	}

	// get scaling
	double DriveStep::get_scaling(const double time) const{
		if(time>tstart_ && time<=tstart_+tpulse_){
			return amplitude_;
		}else{
			return 0;
		}
	}

	// get current derivative
	double DriveStep::get_dscaling(const double /*time*/) const{
		return 0;
	}

	// set amplitude of the pulse
	void DriveStep::set_amplitude(const double amplitude){
		amplitude_ = amplitude;
	}

	// set start time
	void DriveStep::set_tstart(const double tstart){
		tstart_ = tstart;
	}

	// set pulse duration
	void DriveStep::set_tpulse(const double tpulse){
		tpulse_ = tpulse;
	}

	// get times at which an inflection occurs
	arma::Col<double> DriveStep::get_inflection_times() const{
		return arma::Col<double>{tstart_, tstart_ + tpulse_};
	}

	// get type
	std::string DriveStep::get_type(){
		return "rat::mdl::drivestep";
	}

	// method for serialization into json
	void DriveStep::serialize(Json::Value &js, cmn::SList &list) const{
		Drive::serialize(js,list);
		js["type"] = get_type();
		js["amplitude"] = amplitude_;
		js["tstart"] = tstart_;
		js["tpulse"] = tpulse_;
	}

	// method for deserialisation from json
	void DriveStep::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		Drive::deserialize(js,list,factory_list,pth);
		set_amplitude(js["amplitude"].asDouble());
		set_tstart(js["tstart"].asDouble());
		set_tpulse(js["tpulse"].asDouble());
	}

}}