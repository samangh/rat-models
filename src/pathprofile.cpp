/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "pathprofile.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathProfile::PathProfile(){

	}

	// constructor with input
	PathProfile::PathProfile(ShPathPr top, ShPathPr side){
		set_top(top); set_side(side);
	}

	// factory methods
	ShPathProfilePr PathProfile::create(){
		return std::make_shared<PathProfile>();
	}

	// factory with input
	ShPathProfilePr PathProfile::create(ShPathPr top, ShPathPr side){
		return std::make_shared<PathProfile>(top,side);
	}

	// set shearing
	void PathProfile::set_shearing(
		const double alpha, const double pshear, const double ellshear){
		if(pshear<=0)rat_throw_line("shearing parameter must be positive");
		if(ellshear<=0)rat_throw_line("shearing length must be positive");
		alpha_ = alpha; pshear_ = pshear; ellshear_ = ellshear;
	}

	// set top profile
	void PathProfile::set_top(ShPathPr top){
		if(top==NULL)rat_throw_line("top profile points to zero");
		top_ = top;
	}

	// set side profile
	void PathProfile::set_side(ShPathPr side){
		if(side==NULL)rat_throw_line("side profile points to zero");
		side_ = side;
	}


	// get frame
	ShFramePr PathProfile::create_frame() const{
		// check input
		if(top_==NULL)rat_throw_line("top profile not set");
		if(side_==NULL)rat_throw_line("side profile not set");

		// get frame
		ShFramePr top_gen = top_->create_frame();
		ShFramePr side_gen = side_->create_frame();

		// combine
		side_gen->combine();

		// get side profile vectors
		const arma::Mat<double> Rside = side_gen->get_coords(0);
		const arma::Mat<double> Dside = side_gen->get_transverse(0);
		assert(Rside.n_cols>=2); assert(Dside.n_cols>=2);

		// get top profile vectors
		arma::field<arma::Mat<double> > R = top_gen->get_coords();
		arma::field<arma::Mat<double> > L = top_gen->get_direction();
		arma::field<arma::Mat<double> > N = top_gen->get_normal();
		arma::field<arma::Mat<double> > D = top_gen->get_transverse();
		arma::field<arma::Mat<double> > B = top_gen->get_block();

		// interpolate z-direction
		for(arma::uword i=0;i<R.n_elem;i++){
			// temporary array for storing interpolation results
			arma::Col<double> rval;

			// check range
			assert(arma::max(Rside.row(1))>arma::max(R(i).row(1)));
			assert(arma::min(Rside.row(1))<arma::min(R(i).row(1)));

			// interpolate z-coordinate
			arma::interp1(Rside.row(1).t(),Rside.row(2).t(),R(i).row(1).t(),rval,"linear");
			
			// check range
			assert(rval.is_finite());

			// set z-coordinate
			R(i).row(2) = rval.t();

			// set direction vector
			for(arma::uword k=0;k<3;k++){
				arma::interp1(Rside.row(1).t(),Dside.row(k).t(),R(i).row(1).t(),rval,"linear");
				assert(rval.is_finite());
				D(i).row(k) = rval.t();
			}

			// calculate rotation
			const arma::Row<double> alpha = -arma::atan(D(i).row(1)/D(i).row(2));
			assert(alpha.is_finite());

			// rotate longitudinal vector over x-axis
			L(i).row(1) = arma::cos(alpha)%L(i).row(1) - arma::sin(alpha)%L(i).row(2);
			L(i).row(2) = arma::sin(alpha)%L(i).row(1) + arma::cos(alpha)%L(i).row(2);

			// rotate normal vector over x-axis
			N(i).row(1) = arma::cos(alpha)%N(i).row(1) - arma::sin(alpha)%N(i).row(2);
			N(i).row(2) = arma::sin(alpha)%N(i).row(1) + arma::cos(alpha)%N(i).row(2);

			// rotate block vector over x-axis
			B(i).row(1) = arma::cos(alpha)%B(i).row(1) - arma::sin(alpha)%B(i).row(2);
			B(i).row(2) = arma::sin(alpha)%B(i).row(1) + arma::cos(alpha)%B(i).row(2);

			// check if shearing was set
			// this is a feather-M2 feature
			// if it proves useful feel free 
			// to use for your projects
			if(alpha_!=0){
				// add twisting
				const arma::Row<double> as = alpha_*arma::pow(
					arma::clamp((ellshear_ - arma::abs(R(i).row(1)))/ellshear_,0.0,1.0),pshear_); 

				// rotate vectors
				for(arma::uword j=0;j<R(i).n_cols;j++){
					// create rotation matrix
					const arma::Mat<double>::fixed<3,3> M = cmn::Extra::create_rotation_matrix(L(i).col(j),as(j));

					// rotate normal vector but not block vector
					N(i).col(j) = M*N(i).col(j);
					D(i).col(j) = M*D(i).col(j);
				}
			}
		}

		//for(arma::uword i=0;i<B.n_elem;i++)assert(B(i).is_finite());

		// create frame
		ShFramePr gen = Frame::create(R,L,N,D,B);

		// apply transformations
		gen->apply_transformations(trans_);

		// create combined profile
		return gen;
	}

}}