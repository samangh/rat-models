/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "pathgroup.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathGroup::PathGroup(){
		// default orientation
		set_start_coord(arma::Col<double>::fixed<3>{0,0,0}); // origin
		set_start_longitudinal(arma::Col<double>::fixed<3>{0,1,0}); // along y
		set_start_normal(arma::Col<double>::fixed<3>{1,0,0}); // along x
		set_start_transverse(arma::Col<double>::fixed<3>{0,0,1}); // along z
	}

	// factory
	ShPathGroupPr PathGroup::create(){
		return std::make_shared<PathGroup>();
	}

	// constructor with start point input
	PathGroup::PathGroup(
		const arma::Col<double>::fixed<3> R0, 
		const arma::Col<double>::fixed<3> L0, 
		const arma::Col<double>::fixed<3> N0, 
		const arma::Col<double>::fixed<3> D0){
		// set to self
		set_start_coord(R0);
		set_start_longitudinal(L0);
		set_start_normal(N0);
		set_start_transverse(D0);
	}

	// factory with start point
	ShPathGroupPr PathGroup::create(
		const arma::Col<double>::fixed<3> R0, 
		const arma::Col<double>::fixed<3> L0, 
		const arma::Col<double>::fixed<3> N0, 
		const arma::Col<double>::fixed<3> D0){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<PathGroup>(R0,L0,N0,D0);
	}

	// set startpoint
	void PathGroup::set_start_coord(const arma::Col<double>::fixed<3> &R0){
		R0_ = R0;
	}

	// set start direction vector
	void PathGroup::set_start_longitudinal(const arma::Col<double>::fixed<3> &L0){
		// check if it is a unit vector
		if(std::abs(arma::as_scalar(cmn::Extra::vec_norm(L0))-1.0)>1e-7)
			rat_throw_line("longitudinal vector should be of unit length");	

		// set to self
		L0_ = L0;
	}

	// set start normal vector
	void PathGroup::set_start_normal(const arma::Col<double>::fixed<3> &N0){
		// check if it is a unit vector
		if(std::abs(arma::as_scalar(cmn::Extra::vec_norm(N0))-1.0)>1e-7)
			rat_throw_line("normal vector should be of unit length");	

		// set to self
		N0_ = N0;
	}

	// set start transverse vector
	void PathGroup::set_start_transverse(const arma::Col<double>::fixed<3> &D0){
		// check if it is a unit vector
		if(std::abs(arma::as_scalar(cmn::Extra::vec_norm(D0))-1.0)>1e-7)
			rat_throw_line("normal vector should be of unit length");	

		// set to self
		D0_ = D0;
	}

	// set frame
	void PathGroup::set_align_frame(const bool align_frame){
		align_frame_ = align_frame;
	}

	// function for adding a path to the path list
	void PathGroup::add_path(ShPathPr path){
		// check input
		assert(path!=NULL); 

		// get counters
		const arma::uword num_paths = get_num_paths();

		// allocate new source list
		ShPathPrList new_paths(num_paths + 1);

		// set old and new sources
		for(arma::uword i=0;i<num_paths;i++)new_paths(i) = paths_(i);
		new_paths(num_paths) = path;
		
		// set new source list
		paths_ = new_paths;
	}

	// get number of paths
	arma::uword PathGroup::get_num_paths() const{
		return paths_.n_elem;
	}

	// get frame
	ShFramePr PathGroup::create_frame() const{
		// check if paths were set
		assert(!paths_.is_empty());
		
		// get counters
		const arma::uword num_paths = get_num_paths();
		assert(num_paths>0);

		// allocate generator list
		ShFramePrList frame(num_paths);

		// gather frame from all paths
		for(arma::uword i=0;i<num_paths;i++)
			frame(i) = paths_(i)->create_frame();

		// combine frame
		ShFramePr output_gen = Frame::create(frame);

		// align sections
		if(align_frame_)output_gen->align_sections(R0_,L0_,N0_,D0_);

		// apply transformations
		output_gen->apply_transformations(trans_);

		// return the combined frame
		return output_gen;
	}

}}