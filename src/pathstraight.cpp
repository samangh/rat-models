/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "pathstraight.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathStraight::PathStraight(){

	}

	// constructor with specification
	PathStraight::PathStraight(const double length, const double element_size){
		// set to self using respective set methods
		set_length(length); set_element_size(element_size);
	}

	// factory
	ShPathStraightPr PathStraight::create(){
		return std::make_shared<PathStraight>();
	}

	// constructor with dimensions and element size
	ShPathStraightPr PathStraight::create(const double length, const double element_size){
		return std::make_shared<PathStraight>(length,element_size);
	}

	// set path offset
	void PathStraight::set_offset(const double offset){
		offset_ = offset;
	}

	// set length
	void PathStraight::set_length(const double length){
		if(length<=0)rat_throw_line("length must be larger than zero");
		length_ = length;
	}

	// set element size
	void PathStraight::set_element_size(const double element_size){
		if(element_size<=0)rat_throw_line("element size must be larger than zero");
		element_size_ = element_size;
	}

	// set divisor
	void PathStraight::set_element_divisor(const arma::uword element_divisor){
		if(element_divisor<=0)rat_throw_line("divisor must be larger than zero");
		element_divisor_ = element_divisor;
	}

	// darboux generator function
	ShFramePr PathStraight::create_frame() const{
		// check input
		if(length_==0)rat_throw_line("length is not set");
		if(element_size_==0)rat_throw_line("element size is not set");

		// calculate number of nodes for this section
		arma::uword num_nodes = std::max(2,(int)std::ceil(length_/element_size_)+1);
		while((num_nodes-1)%element_divisor_!=0)num_nodes++;

		// allocate coordinates
		arma::Mat<double> R(3,num_nodes,arma::fill::zeros);
		arma::Mat<double> L(3,num_nodes,arma::fill::zeros);
		arma::Mat<double> N(3,num_nodes,arma::fill::zeros);
		arma::Mat<double> D(3,num_nodes,arma::fill::zeros);

		// set vectors for line along x-axis
		R.row(1) = arma::linspace<arma::Row<double> >(0,length_,num_nodes);
		L.row(1).fill(1.0);	// along y
		N.row(0).fill(1.0);	// along x
		D.row(2).fill(1.0); // along z

		// create frame and return
		return Frame::create(R,L,N,D);
	}

}}