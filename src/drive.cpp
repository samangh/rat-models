/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "drive.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// set circuit identifier
	void Drive::set_id(const arma::uword id){
		id_ = id;
	}

	// get identifier
	arma::uword Drive::get_id() const{
		return id_;
	}

	// get type
	std::string Drive::get_type(){
		return "rat::mdl::drive";
	}

	// method for serialization into json
	void Drive::serialize(Json::Value &js, cmn::SList &) const{
		js["type"] = get_type();
		js["drive_id"] = (unsigned int)id_;
	}

	// method for deserialisation from json
	void Drive::deserialize(const Json::Value &js, cmn::DSList &/*list*/, const cmn::NodeFactoryMap &/*factory_list*/, const boost::filesystem::path &/*pth*/){
		set_id(js["drive_id"].asUInt64());
	}

	// get scaling for arrays
	arma::Row<double> Drive::get_scaling(const arma::Row<double> &time) const{
		arma::Row<double> v(time);
		for(arma::uword i=0;i<time.n_elem;i++)v(i)=get_scaling(time(i));
		return v;
	}

	arma::Row<double> Drive::get_dscaling(const arma::Row<double> &time) const{
		arma::Row<double> v(time);
		for(arma::uword i=0;i<time.n_elem;i++)v(i)=get_dscaling(time(i));
		return v;
	}

	// get time array
	arma::Col<double> Drive::get_inflection_times() const{
		return arma::Col<double>{};
	}

}}