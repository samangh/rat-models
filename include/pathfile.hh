/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_PATH_FILE_HH
#define MDL_PATH_FILE_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <functional>

#include "rat/common/defines.hh"

#include "path.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathFile> ShPathFilePr;

	// cross section of coil
	class PathFile: public Path, public Transformations{
		// properties
		private:
			// geometry
      std::string file_name_ = "";
		// methods
		public:
			// constructor
			PathFile();
			PathFile(const std::string& file_name);

			// factory methods
			static ShPathFilePr create();
			static ShPathFilePr create(const std::string& file_name);

			// set properties
			void set_file_name(const std::string& file_name);
			
			// get frame
			virtual ShFramePr create_frame() const;
	};

}}

#endif