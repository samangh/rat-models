/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_PATH_SUB_HH
#define MDL_PATH_SUB_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/defines.hh"
#include "path.hh"
#include "frame.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathSub> ShPathSubPr;

	// cross section of coil
	class PathSub: public Path, public Transformations{
		// properties
		protected:		
			// the base path to derive the offset path from
			ShPathPr base_ = NULL;

			// settings
			arma::uword idx1_;
			arma::uword idx2_;

		// methods
		public:
			// constructor
			PathSub();
			PathSub(ShPathPr base, const arma::uword idx1, const arma::uword idx2);

			// factory
			static ShPathSubPr create();
			static ShPathSubPr create(ShPathPr base, const arma::uword idx1, const arma::uword idx2);

			// set properties
			void set_base(ShPathPr base);
			void set_idx(const arma::uword idx1, const arma::uword idx2);

			// get frame
			virtual ShFramePr create_frame() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth);
	};

}}

#endif
