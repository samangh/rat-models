/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_EMITTER_HH
#define MDL_EMITTER_HH

#include <armadillo> 
#include <memory>

#include "rat/common/node.hh"
#include "rat/common/defines.hh"

#include "particle.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Emitter> ShEmitterPr;

	// emitter template class
	class Emitter: public cmn::Node{
		public:
			// destructor
			virtual ~Emitter(){};

			// particle generator
			virtual arma::field<Particle> spawn_particles(const arma::uword num_particles) const = 0;
	};

}}

#endif