/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_DRIVE_PTRAIN_HH
#define MDL_DRIVE_PTRAIN_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/extra.hh"
#include "drive.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class DrivePTrain> ShDrivePTrainPr;
	typedef arma::field<ShDrivePTrainPr> ShDrivePTrainPrList;

	// circuit class
	class DrivePTrain: public Drive{
		// properties
		protected:
			// amplitude
			double amplitude_ = 1.0;

			// scaling amplitude
			double tstart_ = 1.0; // [s]

			// duration of the pulses
			double tpulse_ = 0.1; // [s]

			// time between the pulses
			double tdelay_ = 100.0; // [s]

			// increment
			double increment_ = 0.5;

			// number of pulses
			arma::uword num_pulses_ = 10;


		// methods
		public:
			// constructor
			DrivePTrain();
			DrivePTrain(const arma::uword num_pulses, const double amplitude, const double increment, const double tpulse, const double tstart, const double tdelay, const arma::uword id = 0);

			// factory
			static ShDrivePTrainPr create();
			static ShDrivePTrainPr create(const arma::uword num_pulses, const double amplitude, const double increment, const double tpulse, const double tstart, const double tdelay, const arma::uword id = 0);

			// set and get definition
			void set_num_pulses(const arma::uword num_pulses);
			void set_amplitude(const double amplitude);
			void set_tstart(const double tstart);
			void set_tpulse(const double tpulse);
			void set_tdelay(const double tdelay);
			void set_increment(const double increment);
						
			// get scaling at given time
			double get_scaling(const double time) const override;
			double get_dscaling(const double time) const override;
			
			// get inflection points
			arma::Col<double> get_inflection_times() const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
