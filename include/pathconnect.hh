/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_PATH_CONNECT_HH
#define MDL_PATH_CONNECT_HH

#include <armadillo> 
#include <cassert>
#include <memory>

#include "path.hh"
#include "frame.hh"
#include "pathbspline.hh" // cubic spline
#include "pathbezier.hh" // quintic spline with constant perimeter

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathConnect> ShPathConnectPr;

	// connects two paths with a connector
	// this then becomes a new combined path
	class PathConnect: public Path{
		// properties
		private:
			// geometric parameters
			ShPathPr prev_path_ = NULL;
			ShPathPr next_path_ = NULL;

			// strength of start and end points
			double str1_;
			double str2_; 
			double str3_; 
			double str4_; 
			double ell_trans_; 
			double element_size_;

		// methods
		public:
			// constructor
			PathConnect();
			PathConnect(ShPathPr prev_path, ShPathPr next_path, const double str1, const double str2, const double str3, const double str4, const double ell_trans, const double element_size);

			// factory methods
			static ShPathConnectPr create();
			static ShPathConnectPr create(ShPathPr prev_path, ShPathPr next_path, const double str1, const double str2, const double str3, const double str4, const double ell_trans, const double element_size);
			
			// set paths
			void set_prev_path(ShPathPr prev_path);
			void set_next_path(ShPathPr next_path);
			void set_strengths(const double str1, const double str2, const double str3, const double str4);
			void set_ell_trans(const double ell_trans);
			void set_element_size(const double element_size);

			// get frame
			virtual ShFramePr create_frame() const;
	};

}}

#endif
