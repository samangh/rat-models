/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_DRIVE_STEP_HH
#define MDL_DRIVE_STEP_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/extra.hh"
#include "drive.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class DriveStep> ShDriveStepPr;
	typedef arma::field<ShDriveStepPr> ShDriveStepPrList;

	// circuit class
	class DriveStep: public Drive{
		// properties
		protected:
			// amplitude
			double amplitude_ = 1.0;

			// scaling amplitude
			double tstart_ = 1.0;

			// phase
			double tpulse_ = 2.0;

		// methods
		public:
			// constructor
			DriveStep();
			DriveStep(const double amplitude, const double tstart, const double tpulse, const arma::uword id = 0);

			// factory
			static ShDriveStepPr create();
			static ShDriveStepPr create(const double amplitude, const double tstart, const double tpulse, const arma::uword id = 0);

			// set and get definition
			void set_amplitude(const double amplitude);
			void set_tstart(const double tstart);
			void set_tpulse(const double tpulse);
			
			// get scaling at given time
			double get_scaling(const double time) const override;
			double get_dscaling(const double time) const override;
			
			// get inflection points
			arma::Col<double> get_inflection_times() const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
