/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_PATH_BEZIER2_HH
#define MDL_PATH_BEZIER2_HH

#include <armadillo>
#include <memory>

#include "rat/common/defines.hh"
#include "rat/common/extra.hh"
#include "rat/mlfmm/extra.hh"
#include "path.hh"
#include "frame.hh"
#include "darboux.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathBezier> ShPathBezierPr;

	// bezier spline implementation
	class PathBezier: public Path{
		// properties
		protected:
			// calculate the frame analytically or numerically
			bool analytic_frame_ = true;
			//bool spline_order_ = 5;
			
			// number of points used for calculating length
			const arma::uword num_reg_ = 20;

			// start point
			arma::Col<double>::fixed<3> pstart_;
			arma::Col<double>::fixed<3> nstart_;
			arma::Col<double>::fixed<3> lstart_;
			
			// end point 
			arma::Col<double>::fixed<3> pend_;
			arma::Col<double>::fixed<3> nend_;
			arma::Col<double>::fixed<3> lend_;

			// strengths
			double str1_;
			double str2_;
			double str3_;
			double str4_;

			// transition length
			double ell_trans_;

			// element size
			double element_size_;

			// use planar windings (winding plane determined by start point)
			bool planar_winding_ = false;

			// path offset
			double path_offset_;

			// number of sections
			arma::uword num_sections_ = 1;

		// methods
		public:
			// constructor
			PathBezier();
			PathBezier(const arma::Col<double>::fixed<3> pstart, const arma::Col<double>::fixed<3> lstart, const arma::Col<double>::fixed<3> nstart,const arma::Col<double>::fixed<3> pend, const arma::Col<double>::fixed<3> lend, const arma::Col<double>::fixed<3> nend, const double str1, const double str2, const double str3, const double str4, const double ell_trans, const double element_size, const double path_offset = 0);
			
			// factory
			static ShPathBezierPr create();
			static ShPathBezierPr create(const arma::Col<double>::fixed<3> pstart, const arma::Col<double>::fixed<3> lstart, const arma::Col<double>::fixed<3> nstart,const arma::Col<double>::fixed<3> pend, const arma::Col<double>::fixed<3> lend, const arma::Col<double>::fixed<3> nend, const double str1, const double str2, const double str3, const double str4, const double ell_trans, const double element_size, const double path_offset = 0);

			// access to properties
			void set_planar_winding(const bool planar_winding);
			void set_analytic_frame(const bool analytic_frame);
			void set_num_sections(const arma::uword num_sections);

			// get frame
			virtual ShFramePr create_frame() const;

			// create regular time array
			arma::Row<double> regular_times(const arma::Mat<double> &P) const;
			
			// bezier function
			static void create_bezier(arma::Mat<double> &R,arma::Mat<double> &V,arma::Mat<double> &A,const arma::Row<double> &t, const arma::Mat<double> &P);

			// recursive bezier function
			static arma::field<arma::Mat<double> > create_bezier_tn(const arma::Row<double> &t, const arma::Mat<double> &P, const arma::uword depth);
	};

}}

#endif
