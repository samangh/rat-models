/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_PARTICLE_HH
#define MDL_PARTICLE_HH

#include <armadillo> 
#include <memory>

#include "rat/common/defines.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// particle and its track
	class Particle{
		protected:
			// properties
			double rest_mass_ = 0; // [eV/C^2] 
			double charge_ = 0; // elementary charge

			// start location
			arma::Col<double>::fixed<3> R0_ = {0,0,0};
			arma::Col<double>::fixed<3> V0_ = {0,0,0};

			// track information
			arma::Row<double> t_;
			arma::Mat<double> Rt_;
			arma::Mat<double> Vt_;
		
			// track orientation with respect to mesh
			// arma::Mat<double> Lt_;
			// arma::Mat<double> Nt_;
			// arma::Mat<double> Dt_;

			// track status
			bool alive_start_ = true;
			bool alive_end_ = true;
			arma::uword num_steps_ = 0;
			arma::uword idx_max_ = 0;
			arma::uword idx_min_ = 0;
			
		public:
			// constructor
			Particle();

			// setting basic properties
			void set_rest_mass(const double rest_mass);
			void set_charge(const double charge);
			
			// get basic particle properties
			double get_rest_mass() const;
			double get_charge() const;

			// set start coordinate
			void set_num_steps(const arma::uword num_steps);
			void set_start_index(const arma::uword start_idx);
			void set_startcoord(const arma::Col<double>::fixed<3> &R0, const arma::Col<double>::fixed<3> &V0);

			// allocate
			void setup();

			// get whether the track is alive
			bool is_alive_start() const;
			bool is_alive_end() const;

			// get the current time, position and velocity
			double get_first_time() const;
			arma::Col<double>::fixed<3> get_first_coord() const;
			arma::Col<double>::fixed<3> get_first_velocity() const;
			double get_last_time() const;
			arma::Col<double>::fixed<3> get_last_coord() const;
			arma::Col<double>::fixed<3> get_last_velocity() const;
			
			// set the next position
			void set_prev_coord(const double t, const arma::Col<double>::fixed<3> &Rp, const arma::Col<double>::fixed<3> &Vp);
			void set_next_coord(const double t, const arma::Col<double>::fixed<3> &Rp, const arma::Col<double>::fixed<3> &Vp);

			// termination
			void terminate_end();
			void terminate_start();

			// get full track
			arma::Mat<double> get_track_coords() const;
			arma::Mat<double> get_track_velocities() const;
	};

}}

#endif