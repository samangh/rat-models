/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_PATH_BEZIER_HH
#define MDL_PATH_BEZIER_HH

#include <armadillo>
#include <memory>

#include "rat/common/extra.hh"
#include "path.hh"
#include "frame.hh"

// code specific to Rat
namespace rat{namespace mdl{
	// shared pointer definition
	typedef std::shared_ptr<class PathBSpline> ShPathBSplinePr;

	// bezier spline implementation
	class PathBSpline: public Path{
		// properties
		protected:
			// settings
			arma::uword Nreg_ = 20;
			double offset_ = 0;
			bool in_plane_ = false; // keeps n-vector in LN-plane

			// start point
			arma::Col<double>::fixed<3> pstart_;
			arma::Col<double>::fixed<3> lstart_;
			arma::Col<double>::fixed<3> nstart_;
			
			// end point 
			arma::Col<double>::fixed<3> pend_;
			arma::Col<double>::fixed<3> lend_;
			arma::Col<double>::fixed<3> nend_;
			
			// strengths
			double str1_;
			double str2_;

			// additional twists
			arma::sword num_twist_ = 0;

			// bezier constant
			const double bc_ = 0.551784;

			// element size
			double element_size_;

		// methods
		public:
			// constructor
			PathBSpline();
			PathBSpline(const arma::Col<double>::fixed<3> pstart, const arma::Col<double>::fixed<3> lstart, const arma::Col<double>::fixed<3> nstart, const arma::Col<double>::fixed<3> pend,  const arma::Col<double>::fixed<3> lend, const arma::Col<double>::fixed<3> nend, const double str1, const double str2, const double element_size, const double offset = 0);
			PathBSpline(const double pstartx, const double pstarty, const double pstartz, const double lstartx, const double lstarty,	const double lstartz, const double nstartx, const double nstarty,	const double nstartz, const double pendx, const double pendy,	const double pendz, const double lendx, const double lendy,	const double lendz, const double nendx, const double nendy,	const double nendz, const double str1, const double str2, const double element_size, const double offset = 0);
			
			// factory
			static ShPathBSplinePr create();
			static ShPathBSplinePr create(const arma::Col<double>::fixed<3> pstart, const arma::Col<double>::fixed<3> lstart, const arma::Col<double>::fixed<3> nstart, const arma::Col<double>::fixed<3> pend,  const arma::Col<double>::fixed<3> lend, const arma::Col<double>::fixed<3> nend, const double str1, const double str2, const double element_size, const double offset = 0);
			static ShPathBSplinePr create(const double pstartx, const double pstarty, const double pstartz, const double lstartx, const double lstarty,	const double lstartz, const double nstartx, const double nstarty,	const double nstartz, const double pendx, const double pendy,	const double pendz, const double lendx, const double lendy,	const double lendz, const double nendx, const double nendy,	const double nendz, const double str1, const double str2, const double element_size, const double offset = 0);

			// get frame
			virtual ShFramePr create_frame() const;

			// set element size 
			void set_element_size(const double element_size);

			// path offset
			void set_offset(const double offset);

			// calculate
			void get_frame_from_time(arma::Mat<double> &R, arma::Mat<double> &L, arma::Mat<double> &N, arma::Mat<double> &D, const arma::Row<double> &t, const bool make_regular) const;
			
			// helper functions
			// calculate direction
			double get_length() const;
			arma::Mat<double> calculate_direction(const arma::Col<double>::fixed<3> &a, const arma::Col<double>::fixed<3> &b, const arma::Col<double>::fixed<3> &c, const arma::Row<double> &t) const;
			arma::Mat<double> calculate_acceleration(const arma::Col<double>::fixed<3> &a, const arma::Col<double>::fixed<3> &b, const arma::Col<double>::fixed<3> &c, const arma::Row<double> &t) const;
			arma::Row<double> calculate_lengths(const arma::Col<double>::fixed<3> &a, const arma::Col<double>::fixed<3> &b, const arma::Col<double>::fixed<3> &c, const arma::Row<double> &t) const;
			arma::Mat<double> calculate_coords(const arma::Col<double>::fixed<3> &a, const arma::Col<double>::fixed<3> &b, const arma::Col<double>::fixed<3> &c, const arma::Row<double> &t) const;
			arma::Row<double> regular_times(const arma::Col<double>::fixed<3> &a, const arma::Col<double>::fixed<3> &b, const arma::Col<double>::fixed<3> &c, const arma::Row<double> &t) const;
			
			// setting
			void set_num_twist(const arma::sword num_twist);
			void set_in_plane(const bool in_plane);
	};

}}

#endif
