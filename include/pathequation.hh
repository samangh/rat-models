/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_PATH_EQUATION_HH
#define MDL_PATH_EQUATION_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <functional>

#include "rat/common/defines.hh"
#include "path.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathEquation> ShPathEquationPr;

	// define the coordinate function
	typedef std::function<void(arma::Mat<double> &R,arma::Mat<double> &L, 
		arma::Mat<double> &D, const arma::Row<double>&)> CoordFun;

	// cross section of coil
	class PathEquation: public Path, public Transformations{
		// properties
		private:
			// number of steps used for evaluating length
			arma::uword num_steps_ini_ = 100; // number of points initially
			arma::uword max_iter_ = 1000;  // maximum number of iterations
			double ell_tol_ = 1e-4; // tolerance for determining the length [m]

			// geometry
			double element_size_ = 0; // size of the elements [m]

			// coordinate function definition R(t) 
			// where t runs from 0 to 1
			CoordFun coord_fun_ = NULL;

			// number of sections
			arma::uword num_sections_ = 1;

			// number of elements must be divisible by this number
			arma::uword element_divisor_ = 1;


		// methods
		public:
			// constructor
			PathEquation();
			PathEquation(const double element_size, CoordFun fun);

			// factory methods
			static ShPathEquationPr create();
			static ShPathEquationPr create(const double element_size, CoordFun fun);

			// set properties
			void set_element_size(const double element_size);
			void set_element_divisor(const arma::uword element_divisor);
			void set_coord_fun(CoordFun fun);
			void set_num_sections(const arma::uword num_sections);

			// get frame
			virtual ShFramePr create_frame() const;

			// function for calculating path length
			double calc_length() const;

			// user coordinate function checking
			void check_user_fun_output(const arma::Mat<double> &R, const arma::Mat<double> &L, const arma::Mat<double> &D, const arma::Row<double> &t) const;
	};

}}

#endif