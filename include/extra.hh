/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef MDL_EXTRA_HH
#define MDL_EXTRA_HH

// general headers
#include <armadillo> 

// code specific to Rat
namespace rat{namespace mdl{

	// serialization code for distmesh models
	class Extra{
		public:
			// function for extracting objects of a certian type from a field
			template<typename T1, typename T2> static arma::field<std::shared_ptr<T1> > filter(const arma::field<std::shared_ptr<T2> > &src);

			// function for combining field
			template<typename T> static arma::field<T> field2mat(const arma::field<arma::field<T> > &fld2);

			// join two fields horizontally
			template<typename T> static arma::field<T> join_horiz(const arma::field<T> &fld1, const arma::field<T> &fld2);
			template<typename T> static arma::field<T> join_vert(const arma::field<T> &fld1, const arma::field<T> &fld2);
	};

	// method for extracting objects of specific type
	// from a shared pointer list
	template<typename T1, typename T2> 
		arma::field<std::shared_ptr<T1> > Extra::filter(
		const arma::field<std::shared_ptr<T2> > &src){

		// allocate
		arma::field<std::shared_ptr<T1> > filtered(src.n_elem,1);

		// check for current sources
		arma::uword cnt = 0;
		arma::Row<arma::uword> remain(src.n_elem,arma::fill::ones);
		for(arma::uword i=0;i<src.n_elem;i++){
			std::shared_ptr<T1> flt = std::dynamic_pointer_cast<T1>(src(i));
			if(flt!=NULL)filtered(cnt++) = flt;
		}

		// return targets
		if(cnt>0)return filtered.rows(0,cnt-1);
		else return arma::field<std::shared_ptr<T1> >(0);
	}

	// merging field arrays containing field arrays
	template<typename T> arma::field<T> Extra::field2mat(
		const arma::field<arma::field<T> > &fld){
		// get sizes of all submatrices
		arma::Mat<arma::uword> num_rows(fld.n_rows,fld.n_cols);
		arma::Mat<arma::uword> num_cols(fld.n_rows,fld.n_cols);
		for(arma::uword i=0;i<fld.n_rows;i++){
			for(arma::uword j=0;j<fld.n_cols;j++){
				num_rows(i,j) = fld(i,j).n_rows;
				num_cols(i,j) = fld(i,j).n_cols;
			}
		}

		// check if submatrices fit
		arma::Col<arma::uword> num_rows_first_col = num_rows.col(0);
		arma::Row<arma::uword> num_cols_first_row = num_cols.row(0);

		// check if matrix fits
		assert(arma::all(arma::all(num_rows_first_col-num_rows.each_col()==0)));
		assert(arma::all(arma::all(num_cols_first_row-num_cols.each_row()==0)));

		// allocate output matrix
		arma::field<T> mt(arma::sum(num_rows_first_col),arma::sum(num_cols_first_row));

		// fill output matrix
		// walk over rows
		arma::uword row_start = 0;
		for(arma::uword i=0;i<fld.n_rows;i++){
			// walk over columns
			arma::uword col_start = 0;
			for(arma::uword j=0;j<fld.n_cols;j++){
				// walk over source rows
				for(arma::uword k=0;k<num_rows(i,j);k++){
					// walk over source columns
					for(arma::uword l=0;l<num_cols(i,j);l++){
						mt(row_start+k, col_start+l) = fld(i,j)(k,l);
					}
				}

				// increment column index
				col_start += num_cols_first_row(j);
			}

			// increment row index
			row_start += num_rows_first_col(i);
		}

		// return output matrix
		return mt;
	}

	// join field arrays of arbitrary type
	template<typename T> arma::field<T> Extra::join_horiz(
		const arma::field<T> &fld1, const arma::field<T> &fld2){
		// count rows and columns
		const arma::uword num_rows = fld1.n_rows; assert(fld2.n_rows==num_rows);
		const arma::uword num_cols = fld1.n_cols + fld2.n_cols;

		// allocate
		arma::field<T> mt(num_rows,num_cols);

		// copy data
		for(arma::uword i=0;i<num_rows;i++){
			for(arma::uword j=0;j<fld1.n_cols;j++){
				mt(i,j) = fld1(i,j);
			}
			for(arma::uword j=0;j<fld2.n_cols;j++){
				mt(i,j+fld1.n_cols) = fld2(i,j);
			}
		}

		// return combined field arrays
		return mt;
	}

	// join field arrays of arbitrary type vertically
	template<typename T> arma::field<T> Extra::join_vert(
		const arma::field<T> &fld1, const arma::field<T> &fld2){
		// count rows and columns
		const arma::uword num_cols = fld1.n_cols; assert(fld2.n_cols==num_cols);
		const arma::uword num_rows = fld1.n_rows + fld2.n_rows;

		// allocate
		arma::field<T> mt(num_rows,num_cols);

		// copy data
		for(arma::uword j=0;j<num_cols;j++){
			for(arma::uword i=0;i<fld1.n_rows;i++){
				mt(i,j) = fld1(i,j);
			}
			for(arma::uword i=0;i<fld2.n_rows;i++){
				mt(i+fld1.n_rows,j) = fld2(i,j);
			}
		}

		// return combined field arrays
		return mt;
	} 

}}

#endif