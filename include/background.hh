/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_BACKGROUND_HH
#define MDL_BACKGROUND_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/node.hh"
#include "rat/mlfmm/background.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Background> ShBackgroundPr;
	typedef arma::field<ShBackgroundPr> ShBackgroundPrList;

	// area carries the mesh of the 
	// cross-sectional area of the coil
	class Background: public fmm::Background, public cmn::Node{
		// properties
		protected:
			// add a drive
			arma::uword drive_id_ = 0;

		// methods
		public:
			// default constructor
			Background();
			
			// destructor
			virtual ~Background(){};

			// constructor with input
			Background(const arma::uword drive_id, const arma::Col<double>::fixed<3> &Hext);

			// factory
			static ShBackgroundPr create();

			// factory
			static ShBackgroundPr create(const arma::uword drive_id, const arma::Col<double>::fixed<3> &Hext);

			// set drive id
			void set_drive_id(const arma::uword drive_id);

			// get drive id
			arma::uword get_drive_id() const;

	};

}}

#endif
