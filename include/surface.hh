/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_SURFACE_HH
#define MDL_SURFACE_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/log.hh"
#include "rat/common/elements.hh"
#include "rat/common/extra.hh"
#include "rat/common/gmshfile.hh"

#include "rat/mlfmm/currentsources.hh"
#include "rat/mlfmm/mgntargets.hh"

#include "trans.hh"
#include "area.hh"
#include "frame.hh"
#include "nameable.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Surface> ShSurfacePr;
	typedef arma::field<ShSurfacePr> ShSurfacePrList;

	// mesh of a coil
	// this objects carries all properties 
	// required for the calculations
	class Surface: public Nameable{
		// properties
		protected:
			// frame
			ShFramePr gen_;

			// cross sectional area
			ShAreaPr area_;

			// mesh
			arma::Mat<double> R_;
			
			// node orientation vectors
			arma::Mat<double> L_;
			arma::Mat<double> N_;
			arma::Mat<double> D_;

			// elements
			arma::Mat<arma::uword> s_; // surface (quad)

			// operating drive
			arma::uword drive_id_;

			// operating conditions
			double operating_temperature_;

		// methods
		public:
			// default constructor
			Surface();
			Surface(const arma::Mat<double> &R, const arma::Mat<double> &L,	const arma::Mat<double> &N, const arma::Mat<double> &D,	const arma::Mat<arma::uword> &s);

			// virtual destructor
			virtual ~Surface(){};

			// factory
			static ShSurfacePr create();
			static ShSurfacePr create(const arma::Mat<double> &R, const arma::Mat<double> &L, const arma::Mat<double> &N, const arma::Mat<double> &D, const arma::Mat<arma::uword> &s);

			// set area 
			void set_area(ShAreaPr area);
			void set_frame(ShFramePr gen);

			// set mesh
			void set_mesh(const arma::Mat<double> &R, const arma::Mat<arma::uword> &s);

			// set temperature
			void set_operating_temperature(const double operating_temperature);

			// set orientation vectors
			void set_longitudinal(const arma::Mat<double> &L);
			void set_transverse(const arma::Mat<double> &D);
			void set_normal(const arma::Mat<double> &N);

			// access to vectors per section
			arma::Mat<double> get_direction() const;
			arma::Mat<double> get_normal() const;
			arma::Mat<double> get_transverse() const;

			// get coordinates and elements
			arma::Mat<double> get_coords() const;
			arma::Mat<arma::uword> get_elements() const;

			// get number of nodes and elements
			arma::uword get_num_nodes() const;
			arma::uword get_num_elements() const;

			// getting operating conditions
			arma::uword get_drive_id() const;

			// setting operating conditions
			void set_drive_id(const arma::uword drive_id);

			// get the current density and temperature at the mesh nodes
			// for a non-coil mesh these output zero matrices
			virtual arma::Mat<double> get_nodal_current_density() const;
			virtual arma::Mat<double> get_nodal_temperature() const;

			// calculate field angle, critical current, critical temperature and electric field
			// the input for this is the magnetic field, temperature and current density at the nodes
			virtual arma::Row<double> calc_alpha(const arma::Mat<double> &B) const;
			virtual arma::Row<double> calc_Je(const arma::Mat<double> &B, const arma::Row<double> &T) const;
			virtual arma::Row<double> calc_Tc(const arma::Mat<double> &J, const arma::Mat<double> &B) const;
			virtual arma::Mat<double> calc_E(const arma::Mat<double> &J, const arma::Mat<double> &B, const arma::Row<double> &T) const;

			
	};

}}

#endif
