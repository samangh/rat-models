/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef COIL_OBJ_HH
#define COIL_OBJ_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "common/defines.hh"
#include "circuit.hh"
#include "coolant.hh"
#include "conductor.hh"
#include "conductornist.hh"

// object containing coil properties
class CoilObj{
	// properties
	protected:
		// powering circuit
		ShCircuitPr circ_ = Circuit::create(0);

		// coolant 
		ShCoolantPr clnt_ = Coolant::create(4.5);

		// material of this coil
		ShConductorPr con_ = ConductorNIST::create();

		// winding geometry
		double number_turns_ = 1;

	// methods
	public:
		// access circuit
		void set_circuit(ShCircuitPr circuit);
		ShCircuitPr get_circuit() const;

		// access coolant
		void set_coolant(ShCoolantPr coolant);
		ShCoolantPr get_coolant() const;

		// set conductor
		void set_conductor(ShConductorPr con);
		ShConductorPr get_conductor() const;

		// getting of number of turns
		void set_number_turns(const double number_turns);
		double get_number_turns() const;
};

#endif
