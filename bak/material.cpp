/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "material.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default critical current is zero (not superconducting)
	arma::Mat<double> Material::calc_critical_current_density(
		const arma::Row<double> &Bm, const arma::Row<double> &/*T*/, const arma::Row<double> &/*alpha*/, const bool /*combined*/) const{

		// create zero matrix and return
		return arma::Mat<double>(1,Bm.n_elem,arma::fill::zeros);
	}

	// default resistivity
	arma::Mat<double> Material::calc_resistivity(const arma::Row<double> &/*Bm*/, const arma::Row<double> &T, const bool /*combined*/) const{
		arma::Mat<double> rho(1,T.n_elem); rho.fill(arma::datum::inf);
		return rho;
	}

	// current density without pre-calculating rho and jc
	arma::Row<double> Material::calc_current_density(const arma::Row<double> &E, const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const{
		// perform pre-calculation
		const arma::Mat<double> rho = calc_resistivity(Bm,T,false);
		const arma::Mat<double> Jc = calc_critical_current_density(Bm,T,alpha,false);

		// call normally
		return calc_current_density(E,Bm,T,alpha,Jc,rho);
	}

	// electric field without pre-calculating rho and jc
	arma::Row<double> Material::calc_electric_field(const arma::Row<double> &J, const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const{
		// perform pre-calculation
		const arma::Mat<double> rho = calc_resistivity(Bm,T,false);
		const arma::Mat<double> Jc = calc_critical_current_density(Bm,T,alpha,false);

		// call normally
		return calc_electric_field(J,Bm,T,alpha,Jc,rho);
	}

	// get factions
	arma::Col<double> Material::get_fraction()const{
		return arma::Col<double>::fixed<1>{1.0};
	}

	// default critical current is zero (not superconducting)
	arma::Row<double> Material::calc_critical_temperature(
		const arma::Row<double> &J, const arma::Row<double> &Bm, 
		const arma::Row<double> &alpha, const ShAreaPr area) const{
		
		// settings
		const double Tinc = 10;
		const double tol = 0.1;

		// allocate critical temperature
		arma::Row<double> Tc_upper = arma::Row<double>(Bm.n_rows,Bm.n_cols,arma::fill::ones)*Tinc;

		// iterations
		for(;;){
			// calculate critical current in [A/m^2]
			const arma::Row<double> Jc = calc_critical_current_density(Bm,Tc_upper,alpha,area);

			// find indexes of elements below Jc
			const arma::Row<arma::uword> idx = arma::find(Jc>J).t();
			
			// check if any left
			if(idx.is_empty())break;

			// increase critical temperature
			Tc_upper(idx)+=Tinc;
		}

		// remove last increment
		arma::Row<double> Tc_lower = Tc_upper - Tinc;

		// bisection algorithm
		for(;;){
			// calculate average
			const arma::Row<double> Tc_mid = (Tc_upper + Tc_lower)/2;

			// calculate critical current in [A/m^2]
			const arma::Row<double> Jc = calc_critical_current_density(Bm,Tc_mid,alpha,area);

			// update temperatures
			const arma::Row<arma::uword> idx1 = arma::find(Jc<J).t();
			Tc_upper(idx1) = Tc_mid(idx1);
			const arma::Row<arma::uword> idx2 = arma::find(Jc>=J).t();
			Tc_lower(idx2) = Tc_mid(idx2);

			// stop condiction
			if(arma::all(arma::abs(Tc_lower - Tc_upper)<tol))break;
		}

		// check output
		assert(arma::all(Tc_upper>0));

		// return critical temp
		return Tc_upper;
	}


	// calculate critical current averaged over the cross-section of the cable
	arma::Row<double> Material::calc_critical_current_density(
		const arma::Row<double> &Bm, const arma::Row<double> &T, 
		const arma::Row<double> &alpha, const ShAreaPr area) const{

		if(area==NULL)return calc_critical_current_density(Bm,T,alpha);

		// calculate critical current normally
		const arma::Row<double> Jen = arma::vectorise(calc_critical_current_density(Bm,T,alpha),1);

		// get counters
		const arma::uword num_nodes_total = Jen.n_elem;
		const arma::uword num_nodes = area->get_num_nodes();
		const arma::uword num_elements = area->get_num_elements();
		const arma::uword num_sections = num_nodes_total/num_nodes; // number of cross sections

		// calculate areas
		const arma::Row<double> A = area->calculate_areas();
		const double Atot = arma::accu(A);
		assert(std::abs(arma::accu(A)-Atot)<1e-9);

		// get elements
		const arma::Mat<arma::uword> n = area->get_elements();
		assert(n.n_rows==4); assert(n.n_cols==num_elements);

		// allocate
		arma::Row<double> Jeav(1,num_nodes_total);

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++){
			// extract section J at nodes
			const arma::Row<double> Jnsection = Jen.cols(i*num_nodes, (i+1)*num_nodes-1);

			// calculate J at elements
			arma::Row<double> Jee(num_elements);
			for(arma::uword j=0;j<num_elements;j++)
				Jee.col(j) = cmn::Quadrilateral::quad2cart(
					Jnsection.cols(n.col(j)),arma::Col<double>::fixed<3>{0,0,0});

			// calculate average current density
			const double Jav = arma::accu(A%Jee)/Atot;

			// set to output
			Jeav.cols(i*num_nodes, (i+1)*num_nodes-1).fill(Jav);
		}

		// return averaged engineering current density
		return Jeav;
	}

	// electric field calculation with scalar input
	double Material::calc_electric_field(const double J, const double Bm, const double T, const double alpha) const{
		// convert to matrices
		arma::Row<double>::fixed<1> Jmat = {J};
		arma::Row<double>::fixed<1> Bmat = {Bm};
		arma::Row<double>::fixed<1> Tmat = {T};
		arma::Row<double>::fixed<1> amat = {alpha};

		// calculate and return value
		return arma::as_scalar(calc_electric_field(Jmat,Bmat,Tmat,amat));
	}

	// export vtk table
	ShVTKTablePr Material::export_vtk(const double Tmin, const double Tmax, const double Jfix) const{
		// create table
		ShVTKTablePr vtk = VTKTable::create();

		// create temperature and field arrays
		arma::Row<double> T = arma::regspace<arma::Row<double> >(Tmin,Tmax);
		arma::Row<double> Bm = arma::Row<double>(T.n_elem,arma::fill::zeros);
		arma::Row<double> alpha = arma::Row<double>(T.n_elem,arma::fill::zeros);
		arma::Row<double> J = arma::Row<double>(T.n_elem,arma::fill::ones)*Jfix;

		// set table size
		vtk->set_num_rows(T.n_elem);

		// add data
		vtk->set_data(T.t(),"temperature [K]");
		vtk->set_data(calc_resistivity(Bm,T).t(),"resistivity [Ohm m]");
		vtk->set_data(calc_thermal_conductivity(Bm,T).t(),"thermal cond. [W m^-1 K^-1]");
		vtk->set_data(calc_specific_heat(T).t(),"specific heat [J m^-3 K^-1]");
		vtk->set_data(calc_electric_field(J,Bm,T,alpha).t(),"electric field [V/m]");

		// return table
		return vtk;
	}
}}