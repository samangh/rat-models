/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_FACTORY_LIST_HH
#define MDL_FACTORY_LIST_HH

#include "rat/common/node.hh"
#include "rat/common/serializer.hh"

#include "rat/mat/factorylist.hh"
#include "rat/dmsh/factorylist.hh"

#include "rat/mlfmm/settings.hh"

#include "crosscircle.hh"
#include "crossdmsh.hh"
#include "crossrectangle.hh"
#include "crosspoint.hh"
#include "crossline.hh"

#include "modelcoil.hh"
#include "modelmesh.hh"
#include "modeltoroid.hh"
#include "modelgroup.hh"

#include "pathaxis.hh"
#include "pathcable.hh"
#include "pathcircle.hh"
#include "pathclover.hh"
#include "pathrectangle.hh"
#include "pathflared.hh"
#include "pathdshape.hh"
#include "patharc.hh"
#include "pathcct.hh"
#include "pathcctcustom.hh"
#include "pathoffset.hh"

#include "transbend.hh"
#include "transreflect.hh"
#include "transflip.hh"
#include "transreverse.hh"
#include "transrotate.hh"
#include "transtranslate.hh"

#include "driveac.hh"
#include "drivedc.hh"
#include "drivetrapz.hh"
#include "driveinterp.hh"

#include "calcgroup.hh"
#include "calcline.hh"
#include "calcharmonics.hh"
#include "calcgrid.hh"
#include "calcmesh.hh"
#include "calcsurface.hh"
#include "calcinductance.hh"
#include "calctracks.hh"
#include "calclength.hh"

#include "emitterbeam.hh"

#include "area.hh"
#include "frame.hh"
#include "mesh.hh"
#include "meshcoil.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// use a class for storage
	class FactoryList{
		public:
			static void register_constructors(rat::cmn::ShSerializerPr slzr){
				// factories from materials
				rat::mat::FactoryList::register_constructors(slzr);

				// factories from distmesh
				rat::dm::FactoryList::register_constructors(slzr);

				// factories from MLFMM
				slzr->register_factory<fmm::Settings>();

				// cross sections
				slzr->register_factory<CrossCircle>();
				slzr->register_factory<CrossDMsh>();
				slzr->register_factory<CrossRectangle>();
				slzr->register_factory<CrossPoint>();
				slzr->register_factory<CrossLine>();

				// model nodes
				slzr->register_factory<ModelCoil>();
				slzr->register_factory<ModelMesh>();
				slzr->register_factory<ModelToroid>();
				slzr->register_factory<ModelGroup>();

				// path nodes
				slzr->register_factory<PathAxis>();
				slzr->register_factory<PathCable>();
				slzr->register_factory<PathCircle>();
				slzr->register_factory<PathClover>();
				slzr->register_factory<PathRectangle>();
				slzr->register_factory<PathFlared>();
				slzr->register_factory<PathDShape>();
				slzr->register_factory<PathArc>();
				slzr->register_factory<PathCCT>();
				slzr->register_factory<PathCCTCustom>(); 
				slzr->register_factory<PathOffset>(); 
				slzr->register_factory<CCTHarmonicInterp>();

				// transformations
				slzr->register_factory<TransBend>();
				slzr->register_factory<TransFlip>();
				slzr->register_factory<TransReflect>();
				slzr->register_factory<TransReverse>();
				slzr->register_factory<TransRotate>();
				slzr->register_factory<TransTranslate>();

				// drives
				slzr->register_factory<DriveAC>();
				slzr->register_factory<DriveDC>();
				slzr->register_factory<DriveTrapz>();
				slzr->register_factory<DriveInterp>();

				// calculation
				slzr->register_factory<CalcGroup>();
				slzr->register_factory<CalcLine>();
				slzr->register_factory<CalcHarmonics>();
				slzr->register_factory<CalcGrid>();
				slzr->register_factory<CalcMesh>();
				slzr->register_factory<CalcSurface>();
				slzr->register_factory<CalcInductance>();
				slzr->register_factory<CalcTracks>();
				slzr->register_factory<CalcLength>();
				slzr->register_factory<EmitterBeam>();

				// data objects
				slzr->register_factory<Area>();
				slzr->register_factory<Frame>();
				slzr->register_factory<Mesh>();
				slzr->register_factory<MeshCoil>();
			}
	};

}}

#endif