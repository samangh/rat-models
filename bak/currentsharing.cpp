/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>

// header files for FX-Models
#include "material.hh"
#include "matgroup.hh"
#include "matnbti.hh"
#include "matcopper.hh"

// main
int main(){
	// settings
	double fnbti = 0.4;
	double fcu = 0.6;

	// create conductor group
	rat::mdl::ShMatGroupPr grp = rat::mdl::MatGroup::create();
	{
		// add nbti superconductor
		rat::mdl::ShMatNbTiPr nbti = rat::mdl::MatNbTi::create();
		nbti->set_nbti_LHC();
		grp->add_material(fnbti,nbti);

		// add copper matrix
		rat::mdl::ShMatCopperPr copper = rat::mdl::MatCopper::create();
		copper->set_copper_OFHC_RRR100();
		grp->add_material(fcu,copper);
	}
	rat::mdl::ShMaterialPr mat = grp;

	// current sharing time
	arma::Row<double> Bm = arma::Row<double>(20,arma::fill::ones)*8;
	arma::Row<double> T = arma::Row<double>(20,arma::fill::ones)*1.9;
	arma::Row<double> alpha = arma::Row<double>(20,arma::fill::zeros);

	// chose critical current density of NbTi
	arma::Row<double> Jop = arma::linspace<arma::Row<double> >(0,2e9,20);

	std::cout<<Jop<<std::endl;

	// calculate electric field
	arma::Row<double> E = mat->calc_electric_field(Jop,Bm,T,alpha);
	
	std::cout<<Jop<<std::endl;

	// output
	std::cout<<E<<std::endl;
}	