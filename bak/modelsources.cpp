/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "modelsources.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelSources::ModelSources(){

	}

	// constructor with model input
	ModelSources::ModelSources(ShModelPr model){
		if(model==NULL)rat_throw_line("model points to NULL");
		add_model(model);
	}

	// factory
	ShModelSourcesPr ModelSources::create(){
		//return ShModelCoilPr(new ModelCoil);
		return std::make_shared<ModelSources>();
	}

	// factory
	ShModelSourcesPr ModelSources::create(ShModelPr model){
		return std::make_shared<ModelSources>(model);
	}

	// setup function
	void ModelSources::setup(cmn::ShLogPr lg){
		// header
		lg->msg(2,"%s%sSETTING UP LINE SOURCES%s\n",KBLD,KGRN,KNRM);

		// create meshes
		ShMeshPrList meshes = create_mesh();
		
		// display meshes
		Mesh::display(lg, meshes);

		// // select only coilmeshes
		// ShMeshCoilPrList cmshes = MeshCoil::extract(meshes);

		// count number of meshes
		//const arma::uword num_coils = cmshes.n_elem;
		
		// allocate sources
		fmm::ShCurrentSourcesPrList srcs(meshes.n_elem);

		// create source objects
		for(arma::uword i=0;i<meshes.n_elem;i++)
			srcs(i) = meshes(i)->create_current_sources();

		// combine current sources
		set_sources(srcs);

		// calculate field at nodes
		set_target_coords(Rs_);

		// done
		lg->msg(-2,"\n");
	}

}}

