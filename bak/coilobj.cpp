/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "coilobj.hh"

// set conductor
void CoilObj::set_circuit(ShCircuitPr circ){
	assert(circ!=NULL);
	circ_ = circ;
}

// get conductor
ShCircuitPr CoilObj::get_circuit() const{
	assert(circ_!=NULL);
	return circ_;
}

// set conductor
void CoilObj::set_coolant(ShCoolantPr clnt){
	assert(clnt!=NULL);
	clnt_ = clnt;
}

// get conductor
ShCoolantPr CoilObj::get_coolant() const{
	assert(clnt_!=NULL);
	return clnt_;
}

// set conductor
void CoilObj::set_conductor(ShConductorPr con){
	assert(con!=NULL);
	con_ = con;
}

// get conductor
ShConductorPr CoilObj::get_conductor() const{
	assert(con_!=NULL);
	return con_;
}

// set number of turns
void CoilObj::set_number_turns(const double number_turns){
	number_turns_ = number_turns;
}

// getting of coil number of turns
double CoilObj::get_number_turns() const{
	return number_turns_;
}