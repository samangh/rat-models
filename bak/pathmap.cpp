/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "pathmap.hh"

// default constructor
PathMap::PathMap(){

}

// constructor with dimension input
PathMap::PathMap(ShPathPr base, ShPathPr path){
	set_base(base); set_path(path);
}

// factory
ShPathMapPr PathMap::create(){
	//return ShPathFlaredPr(new PathFlared);
	return std::make_shared<PathFlared>();
}

// factory with dimension input
ShPathMapPr PathMap::create(ShPathPr base, ShPathPr path){
	return std::make_shared<PathMap>(base, path);
}

// set base path
void PathMap::set_base(ShPathPr base){
	if(base==NULL)throw_line("base points to NULL");
	base = base_;
}

// set target path
void PathMap::set_path(ShPathPr path){
	if(path==NULL)throw_line("path points to NULL");
	path_ = path;
}

// get generators
ShDbGenPr PathMap::create_generators() const{
	// get generators
	ShDbGenPr gen_base = base_->create_generators();
	ShDbGenPr gen_path = path_->create_generators();

	// combine base path
	gen_base->combine();

	// get elements
	arma::Mat<double> R = gen_base->get_coord();
	arma::Mat<double> L = gen_base->get_longitudinal();
	arma::Mat<double> N = gen_base->get_normal();
	arma::Mat<double> D = gen_base->get_transverse();

}