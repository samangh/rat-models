/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef CALCULATE_MESH_HH
#define CALCULATE_MESH_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "common/defines.hh"
#include "path.hh"
#include "pathstraight.hh"
#include "pathgroup.hh"
#include "mlfmm/mgntargets.hh"
#include "common/log.hh"
#include "cross.hh"
#include "vtkunstr.hh"

// VTK headers
#include <vtkSmartPointer.h>
#include <vtkDoubleArray.h>
#include <vtkUnstructuredGrid.h>
#include <vtkXMLUnstructuredGridWriter.h> 
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkCellArray.h>

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcMesh> ShCalcMeshPr;

	// simple mesh calculation
	class CalcMesh: public fmm::MgnTargets{		
		// properties
		protected:
			// mesh base and cross section
			ShPathPr base_;
			ShCrossPr crss_;

			// stored mesh (node coordinates in Rt)
			arma::Mat<arma::uword> n_;

		// methods
		public:
			// constructor
			CalcMesh();
			CalcMesh(ShPathPr base, ShCrossPr crss);

			// factory methods
			static ShCalcMeshPr create();
			static ShCalcMeshPr create(ShPathPr base, ShCrossPr crss);

			// setting of the path
			void set_cross(ShCrossPr crss);
			void set_base(ShPathPr base);
			void set_base(const char axis, const char dir, const double ell, const double x, const double y, const double z, const double dl);

			// calculation
			void setup(cmn::ShLogPr lg = cmn::NullLog::create());

			// export to vtk
			vtkSmartPointer<vtkUnstructuredGrid> create_vtk_ugrid(cmn::ShLogPr lg) const;
			ShVTKUnstrPr export_vtk_mesh(cmn::ShLogPr lg = cmn::NullLog::create()) const;
	};

}}

#endif
