/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <armadillo>
#include <iostream>
#include <cmath>

#include "rat/common/log.hh"
#include "rat/common/defines.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/extra.hh"

#include "rat/mlfmm/soleno.hh"
#include "rat/mlfmm/settings.hh"

#include "pathcircle.hh"
#include "modelcoil.hh"
#include "calcline.hh"
#include "pathaxis.hh"
#include "crossline.hh"

// analytical equation
arma::Row<double> analytic_shell_axis(
	const arma::Row<double> &z, const double R, const double L, const double I){

	// analytical equation
	const arma::Row<double> Bz = (arma::datum::mu_0*I/(2*L))*(
		z/arma::sqrt(R*R + z%z) + (L-z)/arma::sqrt(R*R+(L-z)%(L-z)));

	// return calculate value
	return Bz;
}

// main
int main(){
	// model geometric input parameters
	const double radius = 40e-3; // coil inner radius [m]
	const double delem = 2e-3; // element size [m]
	const double thickness = 1e-4; // loop cross area [m^2]
	const double Iop = 100; // operating current [A]
	const double ell = 0.2; // length of axis [m]
	const arma::uword num_sections = 4; // number of coil sections
	const double tol = 1e-2; // relative tolerance
	const double hcoil = 40e-3; // length of coil [m]
	const arma::uword num_exp = 7;

	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// create a circular path object
	rat::mdl::ShPathCirclePr circle = rat::mdl::PathCircle::create(radius, num_sections, delem);

	// create a rectangular cross section object
	rat::mdl::ShCrossLinePr line = rat::mdl::CrossLine::create(0,-hcoil/2,0,hcoil/2,thickness,delem);

	// create a coil object
	rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(circle, line);
	coil->set_number_turns(1); coil->set_operating_current(Iop);

	// create axis
	rat::mdl::ShPathAxisPr path = rat::mdl::PathAxis::create('z','x',ell,0,0,0,delem);

	// create mlfmm settings
	rat::fmm::ShSettingsPr fmm_settings = rat::fmm::Settings::create();
	fmm_settings->set_num_exp(num_exp);

	// create line calculation
	rat::mdl::ShCalcLinePr cline = rat::mdl::CalcLine::create(coil,path);
	cline->set_field_type("H",3);
	cline->set_fmm_settings(fmm_settings);
	cline->calculate(lg);

	// get result
	arma::Mat<double> Rl = cline->get_coords();
	arma::Mat<double> Bfmm = cline->get_field("B");
	arma::Row<double> Bz = analytic_shell_axis(Rl.row(2)+hcoil/2,radius,hcoil,Iop);
	arma::Row<double> err = arma::abs(Bfmm.row(2)-Bz)/arma::max(arma::abs(Bz));

	// std::cout<<arma::join_horiz(Bfmm.row(2).t(),Bz.t(),err.t())<<std::endl;

	// compare results
	if(arma::any(arma::abs(Bfmm.row(2)-Bz)/arma::max(arma::abs(Bz))>tol))
		rat_throw_line("axial field is outside tolerance");


}