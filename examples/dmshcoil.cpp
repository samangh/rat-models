/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for distmesh-cpp
#include "rat/dmsh/dfrrectangle.hh"
#include "rat/dmsh/dfdiff.hh"
#include "rat/dmsh/dfrectangle.hh"

// header files for FX-Models
#include "crossdmsh.hh"
#include "pathstraight.hh"
#include "modelcoil.hh"
#include "pathcircle.hh"
#include "calcmesh.hh"

// DESCRIPTION
// Example showing how the 2D mesher can be used to 
// draw a mesh for a coil. This allows for making coils
// with irregular cross sections. Refer to distmesh-cpp 
// for more information on how to setup the mesh.

// main
int main(){
	// INPUT SETTINGS
	// settings
	const double element_size = 2e-3; // size of the elements [m]
	const double radius = 0.05; // radius of the coil [m]

	// GEOMETRY SETUP
	// distance function: subtract two rounded rectangles
	rat::dm::ShDistFunPr df = rat::dm::DFDiff::create(
		rat::dm::DFRRectangle::create(-0.02,0.02,-0.01,0.01,2e-3),
		rat::dm::DFRRectangle::create(-0.01,0.01,-0.005,0.005,2e-3));
	
	// create cross section with distance function
	rat::mdl::ShCrossDMshPr cdmsh = rat::mdl::CrossDMsh::create(element_size,df,rat::dm::DFOnes::create());

	// create path
	rat::mdl::ShPathCirclePr pth = rat::mdl::PathCircle::create(radius,4,element_size);

	// create modelcoil
	rat::mdl::ShModelCoilPr model = rat::mdl::ModelCoil::create(pth,cdmsh);

	// create modelmesh
	rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model, model);
	mesh->set_output_dir("./dmsh/");
	mesh->set_output_fname("coil");
	mesh->add_output_type(rat::mdl::ORIENTATION);

	// setup and write
	mesh->setup(); mesh->write();
}