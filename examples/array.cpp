/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// DESCRIPTION
// Minimalist example of a solenoid model

// header files for common
#include "rat/common/log.hh"

// header files for model
#include "pathcircle.hh"
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "modelgroup.hh"
#include "modelarray.hh"
#include "calcmesh.hh"

// main function
int main(){
	// model geometric input parameters
	const double radius = 25e-3/2; // coil inner radius [m]
	const double dcoil = 50e-3; // thickness of the coil [m]
	const double wcable = 12e-3; // width of the cable [m]
	const double delem = 2e-3; // element size [m]
	const arma::uword num_sections = 4; // number of coil sections

	// model operating parameters
	const double operating_current = 2000; // operating current in [A]
	const arma::uword num_turns = 226; // number of turns

	// array grid size and spacing
	const arma::uword num_x = 1, num_y = 1, num_z = 6;
	const double dx = 2.1*(radius+dcoil), dy = 2.1*(radius+dcoil), dz = 13e-3;

	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// create a circular path object
	rat::mdl::ShPathCirclePr circle = rat::mdl::PathCircle::create(radius, num_sections, delem);
	circle->set_offset(dcoil);

	// create a rectangular cross section object
	rat::mdl::ShCrossRectanglePr rectangle = rat::mdl::CrossRectangle::create(0, dcoil, 0, wcable, delem);

	// create coil
	rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(circle, rectangle);
	coil->set_number_turns(num_turns);
	coil->set_operating_current(operating_current);

	// create array
	rat::mdl::ShModelArrayPr model = rat::mdl::ModelArray::create(coil,num_x,num_y,num_z,dx,dy,dz);
	coil->add_translation(0,0,-(dz*num_z)/2);

	// create a source representation for the coil and set them up
	rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model, model);
	mesh->set_output_dir("./array/");
	mesh->set_output_fname("array");

	// perform calculation and write output file
	mesh->calculate_and_write(lg);
}