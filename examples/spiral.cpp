/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files from Common
#include "rat/common/serializer.hh"
#include "rat/common/log.hh"
#include "rat/common/opera.hh"
#include "rat/common/freecad.hh"
#include "rat/common/gmshfile.hh"

// header files for Models
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "pathequation.hh"
#include "calcmesh.hh"

// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true; // calculate field on surface of coil
	const bool run_freecad = true;

	// data storage directory and filename
	const std::string output_dir = "./spiral/";
	const std::string fname = "spiral";

	// parameters
	const double inner_radius = 0.0025;
	const double width = 4e-3;
	const double pitch = 13e-3;
	const double element_size = 0.5e-3;
	const double num_turns = 10;
	const double num_tapes = 40;
	const double dtape = 0.1e-3;

	// temperature and current
	const double Iop = 1000;
	const double Top = 20;


	// both coils
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();

		// create cross section
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(0,dtape,-width/2,width/2,element_size);

	// walk over layers
	for(arma::uword i=0;i<num_tapes;i++){
		// radius of this layer
		const double radius = inner_radius + i*dtape;

		// calculate winding angle
		const double alpha = std::atan(2*arma::datum::pi*radius/pitch);

		// shape function
		// provided by TE for the inner shape of their coils
		rat::mdl::CoordFun spiral_fun = [=](
			arma::Mat<double> &R, arma::Mat<double> &L, arma::Mat<double> &D, const arma::Row<double> &t){

			// calculate in cylindrical
			const arma::Row<double> theta = t*num_turns*2*arma::datum::pi;

			// position vector
			R = arma::join_vert(radius*arma::cos(theta),radius*arma::sin(theta),pitch*theta/(2*arma::datum::pi));

			// direction vector
			L = arma::join_vert(-radius*arma::sin(theta),radius*arma::cos(theta),
				arma::Row<double>(theta.n_elem,arma::fill::ones)*pitch/(2*arma::datum::pi));
			L.each_row() /= rat::cmn::Extra::vec_norm(L);

			// darboux (width) vector
			D = arma::join_vert(
				arma::Mat<double>(2,theta.n_elem,arma::fill::zeros),
				arma::Mat<double>(1,theta.n_elem,arma::fill::ones)/std::sin(alpha));
		};

		// create a spiral path
		rat::mdl::ShPathEquationPr path_spiral = rat::mdl::PathEquation::create(element_size, spiral_fun);
		path_spiral->set_num_sections(num_turns*4);

		// create coil
		rat::mdl::ShModelCoilPr coil1 = rat::mdl::ModelCoil::create(path_spiral, cross_rect);
		coil1->set_operating_current(Iop); coil1->set_operating_temperature(Top);
		coil1->set_number_turns(num_tapes);
		model->add_model(coil1);

		// create coil
		rat::mdl::ShModelCoilPr coil2 = rat::mdl::ModelCoil::create(path_spiral, cross_rect);
		coil2->set_operating_current(Iop); coil2->set_operating_temperature(Top);
		coil2->add_translation(0,0,pitch/2);
		coil2->set_number_turns(num_tapes);
		coil2->add_reverse();
		model->add_model(coil2);
	}

	// CALCULATION AND OUTPUT
	// create log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// calculate field on the coil
	if(run_coil_field){
		// create a source representation for the coil and set them up
		rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model, model);
		mesh->set_output_dir(output_dir);
		mesh->set_output_fname(fname);

		// add to root
		mesh->calculate(lg); mesh->write(lg);
	}

	// write a freecad macro
	if(run_freecad){
		rat::cmn::ShFreeCADPr fc = rat::cmn::FreeCAD::create("spiral.FCMacro","spiral");
		fc->set_num_sub(1);
		model->export_freecad(fc);
	}

}