/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <iostream>
#include <filesystem>

// header files for common
#include "rat/common/log.hh"

// header files for model
#include "rat/mat/baseconductor.hh"
#include "rat/mat/parallelconductor.hh"

#include "serializer.hh"

// main function
int main(){
	// settings
	double temperature = 20;

	// construct conductor
	rat::mat::ShSerializerPr slzr = rat::mdl::Serializer::create("json/materials/htstape/Fujikura_CERN.json");
	rat::mat::ShConductorPr mat = slzr->construct_tree<rat::mat::Conductor>();

	// get properties
	double shm = mat->calc_specific_heat(temperature);

	std::cout<<shm<<std::endl;
}
