/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for FX-MLFMM
#include "rat/mlfmm/extra.hh"
#include "rat/mlfmm/spharm.hh"

// VTK headers
#include <vtkSmartPointer.h>
#include <vtkRectilinearGrid.h>
#include <vtkDoubleArray.h>
#include <vtkXMLRectilinearGridWriter.h>
#include <vtkXMLImageDataWriter.h>
#include <vtkPointData.h>
#include <vtkImageData.h>

// DESCRIPTION
// This example writes all spherical harmonics up to nmax
// to a VTK grid. This is purely intended for educational
// purposes.

// main
int main(){
	// settings
	std::string fname = "spherical_harmonics";
	const int nmax = 8;
	const arma::uword num_x = 100;
	const arma::uword num_y = 100;
	const arma::uword num_z = 100;
	const double x1 = -0.3; const double x2 = 0.3;
	const double y1 = -0.3; const double y2 = 0.3;
	const double z1 = -0.3; const double z2 = 0.3;

	// arrays defining coordinates
	arma::Row<double> xa = arma::linspace<arma::Row<double> >(x1,x2,num_x);
	arma::Row<double> ya = arma::linspace<arma::Row<double> >(y1,y2,num_y);
	arma::Row<double> za = arma::linspace<arma::Row<double> >(z1,z2,num_z);

	// allocate coordinates
	arma::Mat<double> Rt(3,num_x*num_y*num_z);

	// walk over x
	for(arma::uword i=0;i<num_z;i++){
		for(arma::uword j=0;j<num_y;j++){
			// calculate indexes
			const arma::uword idx1 = i*num_x*num_y + j*num_x;
			const arma::uword idx2 = i*num_x*num_y + (j+1)*num_x - 1;

			// set coordinates
			Rt.submat(0,idx1,0,idx2) = xa;
			Rt.submat(1,idx1,1,idx2).fill(ya(j));
			Rt.submat(2,idx1,2,idx2).fill(za(i));
		}
	}

	// convert to spherical
	const arma::Mat<double> Rsph = rat::fmm::Extra::cart2sph(Rt);

	// calculate spherical harmonics
	rat::fmm::Spharm sph(nmax,Rsph);

	// get strength for specified harmonic
	arma::Mat<double> Mr = arma::real(sph.get_all()).t();
	arma::Mat<double> Mi = arma::imag(sph.get_all()).t();
	assert((int)Mr.n_cols==rat::fmm::Extra::polesize(nmax));
	assert((int)Mi.n_cols==rat::fmm::Extra::polesize(nmax));

	// scale with radius
	Mr.each_col()/=(0.1+Rsph.row(0).t());
	Mi.each_col()/=(0.1+Rsph.row(0).t());

	// normalize
	Mr.each_row()/=arma::max(Mr,0);
	Mi.each_row()/=arma::max(Mi,0);

	// calculate spacing
	double dx = (x2-x1)/num_x; 
	double dy = (y2-y1)/num_y;
	double dz = (z2-z1)/num_z;

	// wrap coordinate arrays
	vtkSmartPointer<vtkDoubleArray> xvtk = vtkDoubleArray::New();
	xvtk->SetNumberOfComponents(1); xvtk->SetArray(xa.memptr(), num_x, 1);
	vtkSmartPointer<vtkDoubleArray> yvtk = vtkDoubleArray::New();
	yvtk->SetNumberOfComponents(1); yvtk->SetArray(ya.memptr(), num_y, 1);
	vtkSmartPointer<vtkDoubleArray> zvtk = vtkDoubleArray::New();
	zvtk->SetNumberOfComponents(1); zvtk->SetArray(za.memptr(), num_z, 1);

	// setup grid
	vtkSmartPointer<vtkImageData> voxels = vtkImageData::New();
	voxels->SetDimensions(num_x,num_y,num_z);
	voxels->SetSpacing(dx,dy,dz);
	voxels->SetOrigin(x1,y1,z1);

	// walk over n
	for(int n=0;n<=nmax;n++){
		for(int m=-n;m<=n;m++){
			// wrap to VTK
			{
				vtkSmartPointer<vtkDoubleArray> real_array = vtkDoubleArray::New();
				real_array->SetNumberOfComponents(1);
				real_array->SetArray(Mr.colptr(rat::fmm::Extra::nm2fidx(n,m)), Mr.n_rows, 1);
				const std::string str = "re, n = " + std::to_string(n) + ", m = " + std::to_string(m);
				real_array->SetName(str.c_str());
				voxels->GetPointData()->AddArray(real_array);
			}

			// wrap to VTK
			{
				vtkSmartPointer<vtkDoubleArray> imag_array = vtkDoubleArray::New();
				imag_array->SetNumberOfComponents(1);
				imag_array->SetArray(Mi.colptr(rat::fmm::Extra::nm2fidx(n,m)), Mi.n_rows, 1);
				const std::string str = "im, n = " + std::to_string(n) + ", m = " + std::to_string(m);
				imag_array->SetName(str.c_str());
				voxels->GetPointData()->AddArray(imag_array);
			}
		}
	}

	// Write the file
	vtkSmartPointer<vtkXMLImageDataWriter> writer =  
		vtkSmartPointer<vtkXMLImageDataWriter>::New();
	std::string fname_ext = fname + ".vti";
	writer->SetFileName(fname_ext.c_str());
	writer->SetInputData(voxels);
	writer->Write();

}